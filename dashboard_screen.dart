import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dart_notification_center/dart_notification_center.dart';
import 'package:date_format/date_format.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';

import 'package:lookz_me/api_model/dashboard_api_model.dart';
import 'package:lookz_me/api_model/dashboard_dist_contact_api_model.dart';
import 'package:lookz_me/api_model/near_by_stories_api_model.dart';
import 'package:lookz_me/custom_widgets/qr_code_scanner_widget.dart';
import 'package:lookz_me/custom_widgets/qr_scanner.dart';
import 'package:lookz_me/firestore_methods/user_firestore_methods.dart';
import 'package:lookz_me/helper/api_controller.dart';
import 'package:lookz_me/helper/constants.dart';
import 'package:lookz_me/helper/custom_alert_view.dart';
import 'package:lookz_me/helper/custom_loader.dart';
import 'package:lookz_me/helper/http_services.dart';
import 'package:lookz_me/helper/local_store.dart';
import 'package:lookz_me/helper/navigators.dart';
import 'package:lookz_me/languages/language.dart';
import 'package:lookz_me/languages/locale_constant.dart';
import 'package:lookz_me/list_view_3.dart';
import 'package:lookz_me/main.dart';
import 'package:lookz_me/screens/dashboard/add_bio_screen.dart';
import 'package:lookz_me/screens/dashboard/create_link_screen.dart';
import 'package:lookz_me/screens/dashboard/distance_contacts.dart';
import 'package:lookz_me/screens/dashboard/notification_screen.dart';
import 'package:lookz_me/screens/dashboard/report_user_screen.dart';
import 'package:lookz_me/screens/dashboard/show_my_qr_code.dart';
import 'package:lookz_me/screens/hardware/hardware_list_screen.dart';
import 'package:lookz_me/screens/location/location_user_list_screen.dart';
import 'package:lookz_me/screens/profile/edit_profile/edit_profile_hastag_screen.dart';
import 'package:lookz_me/screens/profile/edit_profile/edit_profile_screen.dart';
import 'package:lookz_me/screens/profile_other_user/other_user_profile.dart';
import 'package:lookz_me/screens/storytelling/story_listing.dart';
import 'package:lookz_me/screens/storytelling/view_storytelling.dart';
import 'package:lookz_me/screens/subscribers/subscribers.dart';

import 'allow_distance_contact.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  int selectedTabIndex = 0;

  //bool isHavingNotification = true;

  bool isHavingLocationNotification = true;
  bool isHavingSubscriberNotification = true;
  bool isHavingHardwareNotification = true;

  List<String> tabSelectedArr = [
    tab_home_on,
    tab_streaming_on,
    tab_add_post_on,
    tab_user_on,
    tab_hardware_on
  ];
  List<String> tabUnSelectedArr = [
    tab_home_off,
    tab_streaming_off,
    tab_add_post_off,
    tab_user_off,
    tab_hardware_off
  ];

  void onItemTapped(int index) async {
    selectedTabIndex = index;
    print("-=-=-=-=- on item tapped");
    print(selectedTabIndex);
    if (selectedTabIndex == 0 || selectedTabIndex == 2) {
      badgeLeftValue = 20000;
    } else {
      double totalWidth = MediaQuery.of(context).size.width;
      totalWidth = totalWidth / 5;
      badgeLeftValue = totalWidth * selectedTabIndex;
      badgeLeftValue = badgeLeftValue + 5;

      // double totalWidth = MediaQuery.of(context).size.width - 20;
      // totalWidth = totalWidth / 500;
      // badgeLeftValue = totalWidth  * 100 * selectedTabIndex;
      // print("badgeLeftValue--- " +  badgeLeftValue.toString());

    }
    setState(() {});
  }

  List _screens = [
    DashboardViewScreen(),
    LocationUserListScreen(),
    StorytellingListing(),
    Subscribers(),
    HardwareListScreen(), //RecordingTest(),
  ];

  double badgeLeftValue = 20000;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: whiteColor,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          body: Stack(
            children: [
              _screens[selectedTabIndex],
              Positioned(
                  bottom: 0,
                  left: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white,
                    height: 45,
                    child: returnBottomTabBar(),
                  )),
              Positioned(
                bottom: 35,
                left: badgeLeftValue,
                child: Container(
                  height: 50,
                  width: (MediaQuery.of(context).size.width - 40) / 5,
                  decoration: ShapeDecoration(
                    color: mainThemeColor,
                    shape: TooltipShapeBorder(arrowArc: 0.0),
                    shadows: [
                      BoxShadow(
                          color: Colors.black26,
                          blurRadius: 4.0,
                          offset: Offset(2, 2))
                    ],
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    // child: Text('Text 22', style: TextStyle(color: Colors.white)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(location_icon,
                            color: Colors.white, height: 18, width: 18),
                        SizedBox(width: 5),
                        Text("11",
                            style: GoogleFonts.openSans(
                              color: Colors.white,
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          //bottomNavigationBar: returnBottomTabBar(),
        ),
      ),
    );
  }

  // -- bottom tab bar menus
  returnBottomTabBar() {
    double totalWidth = MediaQuery.of(context).size.width;
    // totalWidth = totalWidth - 55;
    totalWidth = totalWidth / 5;
    return Padding(
      padding: const EdgeInsets.only(left: 0, right: 0),
      child: Container(
        height: 45,
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            // -- dashboard
            GestureDetector(
              child: Container(
                width: totalWidth,
                color: Colors.transparent,
                child: Center(
                  child: SvgPicture.asset(
                      selectedTabIndex == 0
                          ? tabSelectedArr[0]
                          : tabUnSelectedArr[0],
                      height: 22,
                      width: 22),
                ),
              ),
              onTap: () {
                setState(() {
                  selectedTabIndex = 0;
                  onItemTapped(0);
                });
              },
            ),
            // -- location
            GestureDetector(
              child: Container(
                width: totalWidth,
                color: Colors.transparent,
                child: Center(
                  child: Stack(
                    children: [
                      SvgPicture.asset(
                          selectedTabIndex == 1
                              ? tabSelectedArr[1]
                              : tabUnSelectedArr[1],
                          height: 24,
                          width: 24),
                      isHavingLocationNotification == true
                          ? Positioned(
                              right: 1,
                              top: 1,
                              child: Container(
                                height: 7,
                                width: 7,
                                decoration: BoxDecoration(
                                  color: mainThemeColor,
                                  borderRadius: BorderRadius.circular(3.5),
                                ),
                              ),
                            )
                          : SizedBox()
                    ],
                  ),
                ),
              ),
              onTap: () {
                setState(() {
                  selectedTabIndex = 1;
                  onItemTapped(1);
                });
              },
            ),
            // -- add story
            GestureDetector(
              child: Container(
                width: totalWidth,
                color: Colors.transparent,
                child: Center(
                  child: SvgPicture.asset(
                      selectedTabIndex == 2
                          ? tabSelectedArr[2]
                          : tabUnSelectedArr[2],
                      height: 22,
                      width: 22),
                ),
              ),
              onTap: () {
                setState(() {
                  selectedTabIndex = 2;
                  onItemTapped(2);
                });
              },
            ),
            // -- subscriber
            GestureDetector(
              child: Container(
                width: totalWidth,
                color: Colors.transparent,
                child: Center(
                  child: Stack(
                    children: [
                      SvgPicture.asset(
                          selectedTabIndex == 3
                              ? tabSelectedArr[3]
                              : tabUnSelectedArr[3],
                          height: 22,
                          width: 22),
                      isHavingSubscriberNotification == true
                          ? Positioned(
                              right: 1,
                              top: 1,
                              child: Container(
                                height: 7,
                                width: 7,
                                decoration: BoxDecoration(
                                  color: mainThemeColor,
                                  borderRadius: BorderRadius.circular(3.5),
                                ),
                              ),
                            )
                          : SizedBox()
                    ],
                  ),
                ),
              ),
              onTap: () {
                setState(() {
                  selectedTabIndex = 3;
                  onItemTapped(3);
                });
              },
            ),
            // -- hardware
            GestureDetector(
              child: Container(
                width: totalWidth,
                color: Colors.transparent,
                child: Center(
                  child: Stack(
                    children: [
                      SvgPicture.asset(
                          selectedTabIndex == 4
                              ? tabSelectedArr[4]
                              : tabUnSelectedArr[4],
                          height: 22,
                          width: 22),
                      isHavingHardwareNotification == true
                          ? Positioned(
                              right: 1,
                              top: 1,
                              child: Container(
                                height: 7,
                                width: 7,
                                decoration: BoxDecoration(
                                  color: mainThemeColor,
                                  borderRadius: BorderRadius.circular(3.5),
                                ),
                              ),
                            )
                          : SizedBox()
                    ],
                  ),
                ),
              ),
              onTap: () {
                setState(() {
                  selectedTabIndex = 4;
                  onItemTapped(4);
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}

class DashboardViewScreen extends StatefulWidget {
  @override
  _DashboardViewScreenState createState() => _DashboardViewScreenState();
}
int selectedSectionTab = 0;
class _DashboardViewScreenState extends State<DashboardViewScreen>
    with SingleTickerProviderStateMixin {
  TextStyle defaultStyle =
      GoogleFonts.openSans(color: Colors.black, fontSize: 11.0);
  TextStyle linkStyle =
      GoogleFonts.openSans(color: mainThemeColor, fontSize: 11.5);
  File imageFile;
  UserFirestoreMethods _userFireStoreMethodObj = UserFirestoreMethods();
  DashboardApiModel dashboardDataResponse;
  ScrollController _scrollViewController;
  TabController _tabController;
  bool isHavingSocialLinks = true;
  double bioTextHeight = 20.0;
  final picker = ImagePicker();
  String _selectedImageFile = "";

  Timer locationTimer;
  bool isGettingNearByStories = false;
  static const getScanplatform = const MethodChannel('com.example.getwrapscan');
  var initializationSettingsAndroid =
      new AndroidInitializationSettings('@mipmap/ic_launcher');
  var initializationSettingsIOs = IOSInitializationSettings();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print("disposeeee---   ");
    selectedSectionTab = 0;
    DartNotificationCenter.unsubscribe(observer: 1, channel: "refreshEvent");
    DartNotificationCenter.unregisterChannel(channel: "refreshEvent");
  }


  @override
  void initState() {
    super.initState();
    getNotiObserverEvents();
    LocalStore.shared.currentScreenName = "";
    // -- set up scroll and tab controlllers
    _scrollViewController = new ScrollController();
    _tabController = TabController(vsync: this, initialIndex: 0, length: 2);

    // -- initialize notification methods
    var initSetttings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOs);
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initSetttings,
        onSelectNotification: onSelectNotification);

    // -- dashboard api calling
    Future.delayed(const Duration(milliseconds: 500), () async {
      // Locale myLocale = Localizations.localeOf(context);

      getHomeData(context);
      _getUserLocation();
      locationTimer = new Timer.periodic(
        Duration(seconds: 60),
        (Timer timer) async {
          print("location timer is going onnnnn---- ");
          print(isGettingNearByStories);
          // final result = await getScanplatform.invokeMethod(
          //     'getCustomScanning', '--');
          // print("this is devicesssssss");
          // print(result);
          if (isGettingNearByStories == false) {
            _getUserLocation();
          }
        },
      );
    });
  }

  @override
  void didChangeDependencies() {


    super.didChangeDependencies();
    print('didChangeDependencies(), counter = ');
  }

  NearByStoriesApiModel _nearByStoriesApiModel;

  // -- current user location
  void _getUserLocation() async {
    bool isLocationServiceEnabled = await Geolocator.isLocationServiceEnabled();
    LocationPermission isLocationServiceEnabledss =
        await Geolocator.checkPermission();
    final result =
        await getScanplatform.invokeMethod('getCustomScanning', '--');
    if (isLocationServiceEnabled == false ||
        isLocationServiceEnabledss == LocationPermission.denied ||
        isLocationServiceEnabledss == LocationPermission.deniedForever) {
      LocationPermission permission = await Geolocator.requestPermission();
      isGettingNearByStories = true;
      print("%%%%%%%%%----- ");
      print(result);
      Map<String, String> parms = {
        "latitude": "",
        "longitude": "",
        "devices": result.join(',')
      };
      print("090909090---- " + parms.toString());
      Response nearByStoriesResponse = await ApiController().httpControllerPost(
          HttpServicesList().getStoryWithLocation + LocalStore.shared.userId,
          parms,
          "",
          context);

      isGettingNearByStories = false;

      if (nearByStoriesResponse != null) {
        NearByStoriesApiModel nearByStoriesApiResponse =
            NearByStoriesApiModel.fromJson(
                json.decode(nearByStoriesResponse.body));

        if (nearByStoriesApiResponse.status == true) {
          _nearByStoriesApiModel = nearByStoriesApiResponse;
          if (_nearByStoriesApiModel.data.stories.length != 0) {
            if (LocalStore.shared.currentScreenName != "story_detail") {
              _showNotification();
            }
          }
        } else {
          print("error adding model - dashboard");
        }
      } else {
        print("5");
        print("error while fetching location - dashboard");
      }
    } else {
      var position = await GeolocatorPlatform.instance
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      if (position.longitude != null) {
        isGettingNearByStories = true;
        print("%%%%%%%%% ");
        print(result);
        Map<String, String> parms = {
          "latitude": position.latitude.toString(),
          "longitude": position.longitude.toString(),
          "devices": result.join(',')
        };

        print("090909090 " + parms.toString());
        print("090909090-------- " + LocalStore.shared.userId);
        if(LocalStore.shared.userId == "" || LocalStore.shared.userId == null) {

          print("ohii gallan");
        }
        else {
          print("ohii gallan chkviaaa");
          Response nearByStoriesResponse = await ApiController()
              .httpControllerPost(
              HttpServicesList().getStoryWithLocation +
                  LocalStore.shared.userId,
              parms,
              "",
              //context
              navigatorKey.currentState.context);

          isGettingNearByStories = false;

          if (nearByStoriesResponse != null) {
            NearByStoriesApiModel nearByStoriesApiResponse =
            NearByStoriesApiModel.fromJson(
                json.decode(nearByStoriesResponse.body));
            if (nearByStoriesApiResponse.status == true) {
              _nearByStoriesApiModel = nearByStoriesApiResponse;
              if (_nearByStoriesApiModel.data.stories.length != 0) {
                if (LocalStore.shared.currentScreenName != "story_detail") {
                  _showNotification();
                }
              }
            } else {
              print("error adding model - dashboard");
            }
          } else {
            print("5");
            print("error while fetching location - dashboard");
          }
        }



      }
    }
  }

  // -- handle notification tap
  Future onSelectNotification(String payload) {
    print(payload);
    Future.delayed(Duration(milliseconds: 500), () async {
      await navigatorKey.currentState.push(MaterialPageRoute(
          builder: (BuildContext context) =>
              ViewStorytelling(storyId: payload, userType: true)));
    });
  }

  // - -show notification
  _showNotification() async {
    for (var i = 0; i < _nearByStoriesApiModel.data.stories.length; i++) {
      var android = new AndroidNotificationDetails(
          'id', 'channel ', 'description',
          priority: Priority.high,
          importance: Importance.max,
          channelShowBadge: true,
          enableLights: true,
          enableVibration: true,
          playSound: true,
          showWhen: true);
      var iOS = new IOSNotificationDetails();
      var platform = new NotificationDetails(iOS: iOS, android: android);

      await Future.delayed(Duration(seconds: 1 * i));
      await flutterLocalNotificationsPlugin.show(i, 'Lookz.me',
          _nearByStoriesApiModel.data.stories[i].headline, platform,
          payload: _nearByStoriesApiModel.data.stories[i].id);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: mainThemeColor, child: SafeArea(child: returnDashboardBody()));
  }

  String getSharePointType(String sharePointType, String deviceName) {
    if (sharePointType == "hardware") {
      if (deviceName == "I7") {
        return hardware_icon_2;
      }
      if (deviceName == "E2") {
        return hardware_icon_3;
      }
      if (deviceName == "Plus") {
        return hardware_icon_1;
      }

      // return hardware_2_dashboard_icon;
    } else {
      if (sharePointType == "link") {
        return link_icon;
      } else if (sharePointType == "qr") {
        return barcodeIcon;
      } else if (sharePointType == "location") {
        return location_icon;
      } else if (sharePointType == "distance_contact") {
        return distance_contact_icon;
      }
    }
  }

  // -- dashboard
  returnDashboardBody() {
    return NestedScrollView(
      controller: _scrollViewController,
      // -- top section
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          // -- app bar
          returnAppBar(innerBoxIsScrolled),

          // -- user detail view
          returnUserDetailView(),

          // -- social contact and distance contact tabs
          returnContactsTabs()
        ];
      },

      // -- social contact and distance contact tab bar body view
      body: new TabBarView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          // -- stories list
          dashboardDataResponse == null
              ? Container(
                  color: Colors.grey.shade300,
                )
              : Container(
                  color: Colors.grey.shade300,
                  child: dashboardDataResponse.data.stories.length == 0
                      ? Center(
                          child: GestureDetector(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black45),
                                      borderRadius: BorderRadius.circular(25)),
                                  height: 50,
                                  width: 50,
                                  child: Icon(Icons.add,
                                      size: 30, color: Colors.black45)),
                              SizedBox(height: 5,),
                              Text(
                                  "Social connect",
                                  style: GoogleFonts.openSans(
                                      color: Colors.black45)),
                            ],
                          ),
                          onTap: () {
                            print("navigate");
                            CustomNavigators.pushOnly(context,
                                StorytellingListing(sourceScreen: "dashboard"));
                          },
                        ))
                      : Padding(
                          padding: const EdgeInsets.only(bottom: 45),
                          child: GridView.count(
                            crossAxisCount: 3,
                            children: List.generate(
                                dashboardDataResponse == null
                                    ? 0
                                    : dashboardDataResponse.data.stories.length,
                                (index) {
                              return GestureDetector(
                                child: Center(
                                    child: Padding(
                                  padding: const EdgeInsets.all(1.0),
                                  child: Stack(
                                    children: [
                                      // -- image
                                      Container(
                                        child: CachedNetworkImage(
                                          imageUrl: dashboardDataResponse
                                                      .data
                                                      .stories[index]
                                                      .imageUrl
                                                      .length ==
                                                  0
                                              ? dummy_image_url
                                              : dashboardDataResponse.data
                                                  .stories[index].imageUrl[0],
                                          placeholder: (context, url) =>
                                              CircularProgressIndicator(),
                                          imageBuilder:
                                              (context, imageProvider) =>
                                                  Container(
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),

                                      // -- Position
                                      Positioned(
                                        top: 0,
                                        left: 0,
                                        child: Container(
                                          height: 60,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(
                                            //color: Colors.red,
                                            gradient: LinearGradient(
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                              colors: <Color>[
                                                Colors.black.withOpacity(0.5),
                                                Colors.transparent
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),

                                      // -- play icon and view count
                                      Positioned(
                                          top: 5,
                                          left: 5,
                                          child: Row(
                                            children: [
                                              SvgPicture.asset(
                                                  play_icon_dashboard,
                                                  color: Colors.white,
                                                  height: 15,
                                                  width: 15),
                                              Text(
                                                  " " +
                                                      dashboardDataResponse.data
                                                          .stories[index].views,
                                                  style: GoogleFonts.openSans(
                                                      color: Colors.white))
                                            ],
                                          )),

                                      // -- sharepoint type
                                      Positioned(
                                          top: 5,
                                          right: 5,
                                          child: dashboardDataResponse
                                                      .data
                                                      .stories[index]
                                                      .sharePointType ==
                                                  ""
                                              ? SizedBox()
                                              : SvgPicture.asset(
                                                  getSharePointType(
                                                      dashboardDataResponse
                                                          .data
                                                          .stories[index]
                                                          .sharePointType,
                                                      dashboardDataResponse
                                                          .data
                                                          .stories[index]
                                                          .sharePointDetail
                                                          .hardwareType),
                                                  color: Colors.white,
                                                  height: 15,
                                                  width: 15))
                                    ],
                                  ),
                                )),
                                onTap: () {
                                  CustomNavigators.pushOnly(
                                      context,
                                      ViewStorytelling(
                                          storyId: dashboardDataResponse
                                              .data.stories[index].id));
                                },
                              );
                            }),
                          ),
                        )),

          // -- distance contact list
          dashboardDistanceContactApiModel == null
              ? Container(
                  color: Colors.white,
                )
              : Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 45),
                    child: ListView.builder(
                      itemCount:
                          dashboardDistanceContactApiModel.data.contacts.length,
                      padding: EdgeInsets.zero,
                      itemBuilder: (context, i) {
                        int userIndex = dashboardDistanceContactApiModel
                            .data.users
                            .indexWhere((element) =>
                                element.userId ==
                                dashboardDistanceContactApiModel
                                    .data.contacts[i].otherUserId);

                        // -- userIndex = for the detail of user array
                        // -- i = for the detail of contacts array

                        return GestureDetector(
                          child: Column(
                            children: [
                              // -- list cell
                              dashboardDistanceContactApiModel.data.contacts[i].reportStatus == "0"
                                  ||
                                  dashboardDistanceContactApiModel.data.contacts[i].reportStatus == "5"
                                  ? returnContactTypeCell(userIndex, i)
                                  : returnReportedContactTypeCell(userIndex, i),
                              // -- separator
                              Container(
                                height: 2,
                                color: Colors.white,
                              )
                            ],
                          ),
                          onTap: () {
                            print("distance contact tap tap");
                            print(i);
                            print(dashboardDistanceContactApiModel.data.users[userIndex].blurStatus);
                            print(dashboardDistanceContactApiModel.data.users[userIndex].blurId);
                            print(dashboardDistanceContactApiModel.data.users[userIndex].otherBlurId);
                            print(dashboardDistanceContactApiModel.data.users[userIndex].otherBlurStatus);



                            Navigator.of(context).push(PageRouteBuilder(
                                opaque: false,
                                pageBuilder:
                                    (BuildContext context, _, __) =>
                                    OtherUserProfile(otherUserProfileId: dashboardDistanceContactApiModel
                                        .data.contacts[i].otherUserId,
                                      blurStatus: dashboardDistanceContactApiModel.data.users[userIndex].otherBlurStatus,
                                      contactType: dashboardDistanceContactApiModel.data.contacts[i].type,
                                      daysLeft: dashboardDistanceContactApiModel.data.contacts[i].daysleft,
                                      reportStatus: dashboardDistanceContactApiModel.data.contacts[i].reportStatus,

                                      storyId: dashboardDistanceContactApiModel.data.contacts[i].storyId,
                                      distanceContactId: dashboardDistanceContactApiModel.data.contacts[i].id,
                                      otherDistanceContactId: dashboardDistanceContactApiModel.data.contacts[i].otherDistanceContactId,
                                      sharePointType: dashboardDistanceContactApiModel.data.contacts[i].sharePointType,

                                    ))).
                            then((val) {
                              if (val != null) {
                                getDistanceContacts();
                              }
                            });

                          },
                        );
                      },
                    ),
                  ),
                ),
        ],
        controller: _tabController,
      ),
    );
  }

  returnContactTypeCell(int userIndex, int i) {
    // -- userIndex = for the detail of user array
    // -- i = for the detail of contacts array
    return Container(
        height: 80,
        color: Colors.grey.shade300,
        child: Slidable(
          actionPane: SlidableDrawerActionPane(),
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Container(
              height: 80,
              color: Colors.grey.shade300,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  // -- user image
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Container(
                      color: Colors.transparent,
                      height: 80,
                      width: 65,
                      child: Stack(
                        children: [
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 15),
                              child: CachedNetworkImage(
                                imageUrl: dashboardDistanceContactApiModel
                                            .data.users[userIndex].imageUrl ==
                                        ""
                                    ? dummy_image_url
                                    : dashboardDistanceContactApiModel
                                        .data.users[userIndex].imageUrl,
                                width: 50,
                                height: 50,
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(25)),
                                    border: Border.all(
                                        color: dashboardDistanceContactApiModel.data.contacts[i].type ==
                                                "1"
                                            ? errorColor
                                            : Colors.transparent,
                                        width: 1.5),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                              bottom: 12,
                              left: 2,
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                child: Container(
                                  height: 20,
                                  width: 60,
                                  color: mainThemeColor,
                                  child: Stack(
                                    children: [
                                      SvgPicture.asset(
                                          "assets/login_assets/button_bg.svg",
                                          fit: BoxFit.fill),
                                      Center(
                                        child: dashboardDistanceContactApiModel.data.contacts[i].daysleft == "over"
                                            ? Text(Languages.of(context).over,
                                                style: GoogleFonts.openSans(
                                                    fontSize: 12,
                                                    color: Colors.white))
                                            : Text(
                                                "${dashboardDistanceContactApiModel.data.contacts[i].daysleft} ${dashboardDistanceContactApiModel.data.contacts[i].daysleft == "1" ? Languages.of(context).day : Languages.of(context).days}",
                                                style: GoogleFonts.openSans(
                                                    fontSize: 10,
                                                    color: Colors.white)),
                                      ),
                                    ],
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),

                  // -- username
                  Padding(
                    padding: const EdgeInsets.only(left: 5, bottom: 10),
                    child: Container(
                      height: 55,
                      color: Colors.transparent,
                      width: MediaQuery.of(context).size.width - 180,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              Text(
                                "@" +
                                    dashboardDistanceContactApiModel
                                        .data.users[userIndex].userName,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                softWrap: false,
                                style: GoogleFonts.openSans(fontSize: 12),
                              ),
                              dashboardDistanceContactApiModel.data
                                          .users[userIndex].profileCertified ==
                                      "1"
                                  ? SvgPicture.asset(certified_icon)
                                  : Container(),
                              dashboardDistanceContactApiModel
                                          .data.users[userIndex].muteStatus ==
                                      "1"
                                  ? SvgPicture.asset(mute_icon)
                                  : Container(),
                              Padding(
                                padding: const EdgeInsets.only(left: 2),
                                child: dashboardDistanceContactApiModel.data
                                            .users[userIndex].subscriberKey ==
                                        "1"
                                    ? SvgPicture.asset(group_icon)
                                    : SvgPicture.asset(group_icon,
                                        color: Colors.black),
                              )
                            ],
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 180,
                            child: Text(
                              dashboardDistanceContactApiModel
                                  .data.contacts[i].addedOn,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: false,
                              style: GoogleFonts.openSans(fontSize: 11),
                            ),
                          ),
                          /*Row(
                                                children: [
                                                  SvgPicture.asset(
                                                      location_icon,
                                                      height: 10,
                                                      width: 10,
                                                      color: Colors.black),
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width -
                                                            190,
                                                    child: Text(
                                                      " Pariser Platz, 10117",
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      softWrap: false,
                                                      style:
                                                          GoogleFonts.openSans(
                                                              fontSize: 10),
                                                    ),
                                                  ),
                                                ],
                                              )*/
                        ],
                      ),
                    ),
                  ),

                  // -- Report button
                  dashboardDistanceContactApiModel.data.contacts[i].reportStatus == "0"
                      ?
                  GestureDetector(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: Container(
                              height: 25,
                              width: 80,
                              child: Center(
                                child: Text("Report",
                                    style: GoogleFonts.openSans(
                                        fontSize: 13, color: Colors.black)),
                              ),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 0.5),
                                  borderRadius: BorderRadius.circular(2)),
                            ),
                          ),
                          onTap: () {
                            print("report report");
                            Navigator.of(context)
                                .push(PageRouteBuilder(
                                    opaque: false,
                                    pageBuilder:
                                        (BuildContext context, _, __) =>
                                            ReportUserScreen(
                                              storyId:
                                                  dashboardDistanceContactApiModel
                                                      .data.contacts[i].storyId,
                                              distanceContactId:
                                                  dashboardDistanceContactApiModel
                                                      .data.contacts[i].id,
                                              otherDistanceContactId:
                                                  dashboardDistanceContactApiModel
                                                      .data
                                                      .contacts[i]
                                                      .otherDistanceContactId,
                                              otherUserId:
                                                  dashboardDistanceContactApiModel
                                                      .data
                                                      .contacts[i]
                                                      .otherUserId,
                                              sharePointType:
                                                  dashboardDistanceContactApiModel
                                                      .data
                                                      .contacts[i]
                                                      .sharePointType,
                                            )))
                                .then((val) {
                              if (val != null) {
                                getDistanceContacts();
                              }

                              print("val from next vc -- ");
                              print(val);
                            });
                          },
                        )
                      :
                  Container(
                          height: 25,
                          width: 80,
                        ),
                ],
              ),
            ),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
                caption: Languages.of(context).visible +
                    "\n" +
                    Languages.of(context).mode, //'Visible\nMode',
                color: errorColor,
                iconWidget: SizedBox(),
                onTap: () {
                  CustomAlertViewWidget().showTextAlertViewWithCancel(context, "Lookz.me", "Are your sure you want to "
                      "${dashboardDistanceContactApiModel.data.users[userIndex].blurStatus == "0" ? "blur" : "unblur"} the user",
                      "Yes", "No",
                      okCompletion: (val) async {
                    if (dashboardDistanceContactApiModel.data.users[userIndex].blurStatus == "0") {
                      Navigator.pop(context);
                      blurUser("blurUsers", dashboardDistanceContactApiModel.data.contacts[i].otherUserId, userIndex, context);
                    } else {
                      Navigator.pop(context);
                      String unblurStatus = await unblurUser("blurUsers",dashboardDistanceContactApiModel.data.users[userIndex].blurId);
                      if (unblurStatus == dataSaved) {
                        dashboardDistanceContactApiModel.data.users[userIndex].blurStatus = "0";
                        dashboardDistanceContactApiModel.data.users[userIndex].blurId = "";
                        setState(() {});
                      }
                    }
                  }, cancelCompletion: (val) {
                    Navigator.pop(context);
                      });
                }),
            IconSlideAction(
                caption: dashboardDistanceContactApiModel.data.users[userIndex].muteStatus == "0" ? Languages.of(context).mute : Languages.of(context).un_mute,
                color: Colors.grey.shade400,
                iconWidget: SizedBox(),
                onTap: () {
                  CustomAlertViewWidget().showTextAlertViewWithCancel(
                      context,
                      Languages.of(context).attention,
                      Languages.of(context).want_to_mute,
                      Languages.of(context).yes,
                      Languages.of(context).cancel, okCompletion: (val)  {
                    Navigator.pop(context);

                    Future.delayed(Duration(milliseconds: 500), () async {
                      print("mutes status--- ");
                      print(dashboardDistanceContactApiModel.data.users[userIndex].muteStatus);


                      // -- mute user
                      if (dashboardDistanceContactApiModel.data.users[userIndex].muteStatus == "0") {
                        muteSubscriber("mutededUsers", dashboardDistanceContactApiModel.data.users[userIndex].userId, i, context);
                      }
                      // -- unmute user
                      else {
                        String unmuteStatus = await unmuteSubscribers("mutededUsers", dashboardDistanceContactApiModel.data.users[userIndex].muteId);
                        if (unmuteStatus == dataSaved) {
                          print(dataSaved);
                          dashboardDistanceContactApiModel.data.users[userIndex].muteStatus = "0";
                          setState(() {});
                        }
                      }

                    });

                  }, cancelCompletion: (val) {
                    Navigator.pop(context);
                  });
                }),
            IconSlideAction(
                caption: Languages.of(context).remove,
                color: mainThemeColor,
                iconWidget: SizedBox(),
                onTap: () {
                  CustomAlertViewWidget().showTextAlertViewWithCancel(
                      context,
                      Languages.of(context).attention,
                      Languages.of(context).want_to_remove,
                      Languages.of(context).yes,
                      Languages.of(context).cancel, okCompletion: (val) {
                    print("yes");
                    print(i);
                    Navigator.pop(context);
                    Future.delayed(Duration(milliseconds: 500), () {
                      removeContact(i);
                    });
                  }, cancelCompletion: (val) {
                    print("no");
                    Navigator.pop(context);
                  });
                }),
          ],
        ));
  }

  //======================================
  //              Remove Distance Contact
  //======================================
  removeContact(int i) async {
    print("api removeContact");

    showLoader(context);
    print("1");

    Map<String, String> parms = {
      "distanceContactId": dashboardDistanceContactApiModel.data.contacts[i].id,
      "otherUserId":
          dashboardDistanceContactApiModel.data.contacts[i].otherUserId
    };

    Response removeResponse = await ApiController().httpControllerPost(
        HttpServicesList().deleteReportFromDistContact +
            LocalStore.shared.userId,
        parms,
        "",
        context);
    print("2");
    if (removeResponse != null) {
      final Map<String, String> qrDict = {
        "activeStatus": "0",
      };
      try {
        var ref = await UserFirestoreMethods()
            .firestoreInstance
            .collection("users")
            .doc(LocalStore.shared.userId)
            .collection("distanceContacts")
            .doc(dashboardDistanceContactApiModel.data.contacts[i].id);
        ref.update(qrDict);

        var ref2 = await UserFirestoreMethods()
            .firestoreInstance
            .collection("users")
            .doc(dashboardDistanceContactApiModel.data.contacts[i].otherUserId)
            .collection("distanceContacts")
            .doc(dashboardDistanceContactApiModel
                .data.contacts[i].otherDistanceContactId);
        ref2.update(qrDict);

        hideLoader(context);

        print("2 *-*-*-** 2");
        print(dashboardDistanceContactApiModel.data.contacts.length);

        Future.delayed(Duration(milliseconds: 500), () {
          dashboardDistanceContactApiModel.data.contacts.removeAt(i);
          setState(() {
            print("3 *-*-*-** 3");
            print(dashboardDistanceContactApiModel.data.contacts.length);
          });
        });
      } catch (error) {
        hideLoader(context);
        print("error distanceContacts");
        print(error.toString());
      }
    } else {
      print("5");
      hideLoader(context);
      CustomAlertViewWidget().showSnackBar(context, "Please try again");
    }
  }

  //======================================
  //                       MUTE SUBSCRIBER
  //======================================
  Future<String> muteSubscriber(String tableName, String otherUserId, int index, BuildContext cntxt) async {
    final Map<String, String> qrDict = {
      // "addedOn": DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()),
      "addedOn": formatDate(DateTime.now(), [yyyy,'-',mm,'-',dd, ' ',hh, ':', mm, ':',ss]),
      "addedOnTimeStamp": DateTime.now().millisecondsSinceEpoch.toString(),
      "otherUserId": otherUserId,
      "userId": LocalStore.shared.userId,
    };

    print("new format---   "  + qrDict.toString());

    try {
      var ref = await UserFirestoreMethods()
          .firestoreInstance
          .collection(tableName)
          .doc();
      ref.set(qrDict);
      Future.delayed(const Duration(milliseconds: 500), () {
        dashboardDistanceContactApiModel.data.users[index].muteStatus = "1";
        dashboardDistanceContactApiModel.data.users[index].muteId = ref.id;
        setState(() {});


      });
    } catch (error) {
      hideLoader(cntxt);
      Future.delayed(const Duration(milliseconds: 500), () {
        CustomAlertViewWidget()
            .showSnackBar(cntxt, "Something went wrong please try again");
      });
    }
  }

  //======================================
  //                     UNMUTE SUBSCRIBER
  //======================================
  Future<String> unmuteSubscribers(String tableName, String tableId) async {
    showLoader(context);
    var statusString = await _userFireStoreMethodObj.deleteDataToTableWithCustomId(tableName, tableId);
    hideLoader(context);
    return statusString;
  }

  returnReportedContactTypeCell(int userIndex, int i) {
    // -- userIndex = for the detail of user array
    // -- i = for the detail of contacts array

    // -- static checks
    if (i == 2) {
      contactIsReportedMe = true;
    } else if (i == 0) {
      contactIsReportedMe = false;
    }

    return Container(
        height: 75,
        color: mainThemeColor,
        child: Slidable(
          actionPane: SlidableDrawerActionPane(),
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Container(
                    height: 60,
                    color: mainThemeColor,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // -- user image
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Container(
                            color: Colors.transparent,
                            height: 80,
                            width: 65,
                            child: Stack(
                              children: [
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: CachedNetworkImage(
                                      imageUrl: dashboardDistanceContactApiModel
                                                  .data
                                                  .users[userIndex]
                                                  .imageUrl ==
                                              ""
                                          ? dummy_image_url
                                          : dashboardDistanceContactApiModel
                                              .data.users[userIndex].imageUrl,
                                      width: 50,
                                      height: 50,
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      imageBuilder: (context, imageProvider) =>
                                          Container(
                                        height: 50,
                                        width: 50,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(25)),
                                          border: Border.all(
                                              color:
                                                  dashboardDistanceContactApiModel
                                                              .data
                                                              .contacts[i]
                                                              .type ==
                                                          "1"
                                                      ? errorColor
                                                      : Colors.transparent,
                                              width: 1.5),
                                          image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        // -- username
                        Padding(
                          padding: const EdgeInsets.only(left: 5, bottom: 10),
                          child: Container(
                            height: 55,
                            color: Colors.transparent,
                            width: MediaQuery.of(context).size.width - 180,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      "@" +
                                          dashboardDistanceContactApiModel
                                              .data.users[userIndex].userName,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: GoogleFonts.openSans(fontSize: 12),
                                    ),
                                    dashboardDistanceContactApiModel
                                                .data
                                                .users[userIndex]
                                                .profileCertified ==
                                            "1"
                                        ? SvgPicture.asset(certified_icon)
                                        : Container(),
                                    dashboardDistanceContactApiModel.data
                                                .users[userIndex].muteStatus ==
                                            "1"
                                        ? SvgPicture.asset(mute_icon)
                                        : Container(),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 2),
                                      child: dashboardDistanceContactApiModel
                                                  .data
                                                  .users[userIndex]
                                                  .subscriberKey ==
                                              "1"
                                          ? SvgPicture.asset(group_icon)
                                          : SvgPicture.asset(group_icon,
                                              color: Colors.black),
                                    )
                                  ],
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width - 180,
                                  child: Text(
                                    dashboardDistanceContactApiModel
                                        .data.contacts[i].reportMessage,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    softWrap: false,
                                    style: GoogleFonts.openSans(fontSize: 11),
                                  ),
                                ),
                                /*Row(
                                                      children: [
                                                        SvgPicture.asset(
                                                            location_icon,
                                                            height: 10,
                                                            width: 10,
                                                            color: Colors.black),
                                                        Container(
                                                          width:
                                                              MediaQuery.of(context)
                                                                      .size
                                                                      .width -
                                                                  190,
                                                          child: Text(
                                                            " Pariser Platz, 10117",
                                                            overflow:
                                                                TextOverflow.ellipsis,
                                                            maxLines: 1,
                                                            softWrap: false,
                                                            style:
                                                                GoogleFonts.openSans(
                                                                    fontSize: 10),
                                                          ),
                                                        ),
                                                      ],
                                                    )*/
                              ],
                            ),
                          ),
                        ),

                        // -- Report button
                        Container(
                          height: 25,
                          width: 80,
                        )

                        // GestureDetector(
                        //   child: Padding(
                        //     padding:
                        //     const EdgeInsets.only(right: 10),
                        //     child: Container(
                        //       height: 25,
                        //       width: 80,
                        //       child: Center(
                        //         child: Text("Report",
                        //             style: GoogleFonts.openSans(
                        //                 fontSize: 13,
                        //                 color: Colors.black)),
                        //       ),
                        //       decoration: BoxDecoration(
                        //           border: Border.all(
                        //               color: Colors.black,
                        //               width: 0.5),
                        //           borderRadius:
                        //           BorderRadius.circular(2)),
                        //     ),
                        //   ),
                        //   onTap: () {
                        //     print("report report");
                        //     Navigator.of(context)
                        //         .push(PageRouteBuilder(
                        //         opaque: false,
                        //         pageBuilder:
                        //             (BuildContext context, _,
                        //             __) =>
                        //             ReportUserScreen()))
                        //         .then((val) {
                        //       print("val from next vc -- ");
                        //       print(val);
                        //     });
                        //   },
                        // ),
                      ],
                    ),
                  ),
                ),
              ),
              dashboardDistanceContactApiModel.data.contacts[i].type == "1"
                  ? Positioned(
                      top: -2,
                      left: 35,
                      child: SvgPicture.asset("assets/dashboard/up_icon.svg",
                          height: 13, width: 13, color: Colors.grey.shade300),
                    )
                  : Positioned(
                      top: -2,
                      left: 35,
                      child: SvgPicture.asset("assets/dashboard/down_icon.svg",
                          height: 13, width: 13, color: Colors.grey.shade300),
                    )
            ],
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
                caption: 'Visible\nMode',
                color: errorColor,
                iconWidget: SizedBox(),
                onTap: () {}),
            IconSlideAction(
                caption: 'Mute',
                color: Colors.grey.shade400,
                iconWidget: SizedBox(),
                onTap: () {
                  CustomAlertViewWidget().showTextAlertViewWithCancel(
                      context,
                      "Attention",
                      "Are you sure you want to mute the user?",
                      "Yes",
                      "Cancel", okCompletion: (val) {
                    Navigator.pop(context);
                  }, cancelCompletion: (val) {
                    Navigator.pop(context);
                  });
                }),
            IconSlideAction(
                caption: 'Remove',
                color: mainThemeColor,
                iconWidget: SizedBox(),
                onTap: () {
                  CustomAlertViewWidget().showTextAlertViewWithCancel(
                      context,
                      "Attention",
                      "Are you sure you want to remove the user?",
                      "Yes",
                      "Cancel", okCompletion: (val) {
                    Navigator.pop(context);
                  }, cancelCompletion: (val) {
                    Navigator.pop(context);
                  });
                }),
          ],
        ));
  }

  // -- top app bar with logo and options
  returnAppBar(bool isForceElevated) {
    return SliverAppBar(
      centerTitle: true,
      elevation: 0.0,
      titleSpacing: 0,
      title: SvgPicture.asset(appBarLogo),
      leadingWidth: 130,
      leading: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Row(
          children: [
            GestureDetector(
              child: Container(
                  color: Colors.transparent,
                  height: 40,
                  width: 40,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SvgPicture.asset(barcodeIcon),
                  )),
              onTap: () async {
                Navigator.of(context)
                    .push(PageRouteBuilder(
                        opaque: false,
                        pageBuilder: (BuildContext context, _, __) =>
                            QRViewExample()))
                    .then((result) {
                  if (result != null) {
                    // ------------------------- Scan other user profile----------------------------
                    if (result.toString().contains("**")) {
                      Future.delayed(const Duration(milliseconds: 100), () {
                        final tagName = result.toString();
                        final split = tagName.split('**');
                        final Map<int, String> values = {
                          for (int i = 0; i < split.length; i++) i: split[i]
                        };
                        if (values[1].toString() != LocalStore.shared.userId) {
                          Navigator.of(context).push(PageRouteBuilder(
                              opaque: false,
                              pageBuilder: (BuildContext context, _, __) =>
                                  OtherUserProfile(
                                      otherUserId: values[1].toString(),
                                      otherUserProfileId: values[1].toString())));
                        }
                      });
                    }

                    // ------------------------- Scan other user Story----------------------------

                    if (result.toString().contains("-Distance-")) {
                      Future.delayed(const Duration(milliseconds: 100), () {
                        showLoader(context);
                        UserFirestoreMethods()
                            .firestoreInstance
                            .collection("stories")
                            .where("share_point_details.sharePointLink",
                                isEqualTo: result.toString())
                            .get()
                            .then((event) async {
                          // -- if doesn't match
                          if (event.docs.length == 0) {
                            hideLoader(context);
                            print("nnnnnnnnnn");
                          } else {
                            hideLoader(context);
                            print("hhhhhhhhhh");
                            print(event.docs.first.id);

                            Future.delayed(Duration(milliseconds: 100),
                                () async {
                              await navigatorKey.currentState.push(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ViewStorytelling(
                                              storyId: event.docs.first.id
                                                  .toString(),
                                              userType: true)));
                            });
                          }
                        });
                      });
                    }
                  }
                });

                // QRScannerCls.scan(completion: (result) {
                //   if (result != null) {
                //     print(result.toString());
                //     Future.delayed(const Duration(milliseconds: 100), () {
                //       final tagName = result.toString();
                //       final split = tagName.split('**');
                //       final Map<int, String> values = {
                //         for (int i = 0; i < split.length; i++) i: split[i]
                //       };
                //
                //       if (values[1].toString() != LocalStore.shared.userId) {
                //         Navigator.of(context).push(PageRouteBuilder(
                //             opaque: false,
                //             pageBuilder: (BuildContext context, _, __) =>
                //                 OtherUserProfile(
                //                     otherUserId: values[1].toString(),
                //                     otherUserProfileId: values[1].toString())));
                //       }
                //     });
                //   }
                // });

                /*
                final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => QrScanner()));
                if (result != null) {
                  print(result.toString());
                  Future.delayed(const Duration(milliseconds: 100), () {
                    final tagName = result.toString();
                    final split = tagName.split('**');
                    final Map<int, String> values = {
                      for (int i = 0; i < split.length; i++) i: split[i]
                    };

                    Navigator.of(context).push(PageRouteBuilder(
                        opaque: false,
                        pageBuilder: (BuildContext context, _, __) =>
                            OtherUserProfile(
                                otherUserId: values[1].toString(),
                                otherUserProfileId: values[1].toString())));
                  });
                }
                */
              },
            ),
            GestureDetector(
              child: Container(
                height: 40,
                width: 40,
                color: Colors.transparent,
                child: Center(
                  child: Stack(
                    children: [
                      SvgPicture.asset(bell_icon, height: 28, width: 28),
                      Positioned(
                        right: 1,
                        top: 1,
                        child: Container(
                          height: 12,
                          width: 12,
                          decoration: BoxDecoration(
                            color: mainThemeColor,
                            borderRadius: BorderRadius.circular(6),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              onTap: () async {
                CustomNavigators.pushOnly(context, NotificationScreen());
              },
            ),
          ],
        ),
      ),
      expandedHeight: 5,
      backgroundColor: Colors.white,
      pinned: true,
      floating: false,
      forceElevated: isForceElevated,
      actions: [
        GestureDetector(
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SvgPicture.asset(hashTagImage),
          ),
          onTap: () async {
            print("hashTagImage");
            CustomNavigators.pushOnly(context, EditProfileHashtagScreen());
          },
        ),
        GestureDetector(
          child: Padding(
            padding: const EdgeInsets.only(right: 12),
            child: SvgPicture.asset(distance_contact_icon),
          ),
          onTap: () {
            print("distance_contact_icon");
            CustomNavigators.pushOnly(context, ShowMyQrCodeScreen());
          },
        ),
      ],
    );
  }

  // -- user detail view
  returnUserDetailView() {
    return SliverToBoxAdapter(
      child: Container(
        height: isHavingSocialLinks ? 342 + bioTextHeight : 297 + bioTextHeight,
        color: Colors.white,
        child: Column(
          children: [
            // -- top image section
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                height: 80,
                child: Center(
                  child: Stack(
                    children: [
                      // -- user image
                      CachedNetworkImage(
                        imageUrl: dashboardDataResponse == null ||
                                dashboardDataResponse
                                        .data.userDetails.userImage ==
                                    ""
                            ? dummy_image_url
                            : dashboardDataResponse.data.userDetails.userImage,
                        width: 80,
                        height: 80,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        imageBuilder: (context, imageProvider) => Container(
                          height: 80,
                          width: 80,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(40)),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      // -- Add icon ( image: galllery : camera )
                      Positioned(
                          bottom: 1,
                          right: 2,
                          child: GestureDetector(
                            child: Container(
                              height: 20,
                              width: 20,
                              child: SvgPicture.asset(add_icon),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            onTap: () async {
                              var connectivityResult =
                                  await (Connectivity().checkConnectivity());
                              if (connectivityResult ==
                                  ConnectivityResult.none) {
                                CustomAlertViewWidget().showSnackBar(
                                    context, internet_connection_error);
                              } else {
                                showCupertinoModalPopup(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      CupertinoActionSheet(
                                    title: Text(
                                        Languages.of(context).choose_option),
                                    //  message: const Text('Your options are '),
                                    actions: <Widget>[
                                      CupertinoActionSheetAction(
                                        child:
                                            Text(Languages.of(context).camera),
                                        onPressed: () {
                                          //Navigator.pop(context, 'One');
                                          getCameraImage(context);
                                        },
                                      ),
                                      CupertinoActionSheetAction(
                                        child:
                                            Text(Languages.of(context).gallery),
                                        onPressed: () {
                                          pickImageFromGallery(context);
                                          //Navigator.pop(context, 'Two');
                                        },
                                      ),
                                      CupertinoActionSheetAction(
                                        child:
                                            Text(Languages.of(context).cancel),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                );
                              }
                            },
                          ))
                    ],
                  ),
                ),
              ),
            ),

            // -- username,
            Padding(
              padding: const EdgeInsets.only(top: 7, bottom: 3),
              child: Text(
                  dashboardDataResponse == null
                      ? ""
                      : "@" + dashboardDataResponse.data.userDetails.userName,
                  style: GoogleFonts.openSans(fontSize: 14)),
            ),

            // -- first name and last name
            Text(
                dashboardDataResponse == null
                    ? ""
                    : dashboardDataResponse.data.userDetails.firstName +
                        " " +
                        dashboardDataResponse.data.userDetails.lastName,
                style: GoogleFonts.openSans(fontSize: 14)),

            // -- storytelling / views / likes
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                width: MediaQuery.of(context).size.width - 60,
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 3 - 20,
                      child: Column(
                        children: [
                          Text(
                              dashboardDataResponse == null
                                  ? ""
                                  : dashboardDataResponse.data.storyTelling,
                              style: GoogleFonts.openSans(fontSize: 17)),
                          Text(Languages.of(context).storytellings,
                              style: GoogleFonts.openSans(
                                  fontSize: 11, color: Colors.grey)),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 3 - 20,
                      child: Column(
                        children: [
                          Text(
                              dashboardDataResponse == null
                                  ? ""
                                  : dashboardDataResponse.data.views,
                              style: GoogleFonts.openSans(fontSize: 17)),
                          Text(Languages.of(context).views,
                              style: GoogleFonts.openSans(
                                  fontSize: 11, color: Colors.grey)),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 3 - 20,
                      child: Column(
                        children: [
                          Text(
                              dashboardDataResponse == null
                                  ? ""
                                  : dashboardDataResponse.data.likes,
                              style: GoogleFonts.openSans(fontSize: 17)),
                          Text(Languages.of(context).likes,
                              style: GoogleFonts.openSans(
                                  fontSize: 11, color: Colors.grey)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // -- Edit Profile button
            GestureDetector(
              child: Container(
                height: 28,
                width: MediaQuery.of(context).size.width - 40,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey.shade300, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                    child: Text(Languages.of(context).edit_profile,
                        style: GoogleFonts.openSans(
                            fontSize: 14, fontWeight: FontWeight.w600))),
              ),
              onTap: () {
                Navigator.of(context)
                    .push(PageRouteBuilder(
                        opaque: false,
                        pageBuilder: (BuildContext context, _, __) =>
                            EditProfileScreen()))
                    .then((val) {
                  print("909090090   " + val.toString());
                  if (val != null) {
                    if (val == true) {
                      getHomeData(context);
                    }
                  }
                }); // => val ? getHomeData(context) : null);
                //CustomNavigators.pushOnly(context, GetBlueDevices());
              },
            ),

            // -- bio
            Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 5),
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Container(
                  height: bioTextHeight,
                  width: MediaQuery.of(context).size.width - 30,
                  child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        WidgetSpan(
                            child: GestureDetector(
                          child: SvgPicture.asset(edit_tab_icon,
                              height: 15, width: 15),
                          onTap: () {
                            Navigator.of(context)
                                .push(PageRouteBuilder(
                                    opaque: false,
                                    pageBuilder:
                                        (BuildContext context, _, __) =>
                                            AddBioScreen(
                                                bioText: dashboardDataResponse
                                                    .data.userDetails.bio)))
                                .then(
                                    (val) => val ? getHomeData(context) : null);
                          },
                        )),
                        WidgetSpan(
                          child: Container(
                            height: bioTextHeight,
                            child: Text(
                                dashboardDataResponse == null
                                    ? Languages.of(context).add_your_bio
                                    : dashboardDataResponse
                                                .data.userDetails.bio ==
                                            null
                                        ? Languages.of(context).add_your_bio
                                        : dashboardDataResponse
                                            .data.userDetails.bio,
                                overflow: TextOverflow.clip,
                                textAlign: TextAlign.center),
                          ),
                        )
                      ])),

                  /*
                  Row(
                    children: [
                      GestureDetector(
                        child: SvgPicture.asset(edit_tab_icon,
                            height: 15, width: 15),
                        onTap: () {
                          Navigator.of(context)
                              .push(PageRouteBuilder(
                              opaque: false,
                              pageBuilder:
                                  (BuildContext context, _, __) =>
                                  AddBioScreen(
                                      bioText: dashboardDataResponse
                                          .data.userDetails.bio)))
                              .then(
                                  (val) => val ? getHomeData(context) : null);
                        },
                      ),
                      Container(
                        height: bioTextHeight,
                        width: MediaQuery.of(context).size.width - 70,
                        //color: Colors.red,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                              dashboardDataResponse == null
                                  ? "Add your bio"
                                  : dashboardDataResponse
                                  .data.userDetails.bio ==
                                  ""
                                  ? "Add your bio"
                                  : dashboardDataResponse
                                  .data.userDetails.bio,
                              overflow: TextOverflow.clip,
                              textAlign: TextAlign.left),
                        ),
                      ),
                    ],
                  )
                    */
                  /*
                  RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        WidgetSpan(
                            child: GestureDetector(
                          child: SvgPicture.asset(edit_tab_icon,
                              height: 15, width: 15),
                          onTap: () {
                            Navigator.of(context)
                                .push(PageRouteBuilder(
                                    opaque: false,
                                    pageBuilder:
                                        (BuildContext context, _, __) =>
                                            AddBioScreen(
                                                bioText: dashboardDataResponse
                                                    .data.userDetails.bio)))
                                .then(
                                    (val) => val ? getHomeData(context) : null);
                          },
                        )),
                        TextSpan(
                          text: " ",
                          style: linkStyle,
                        ),
                        WidgetSpan(
                          child: Container(
                            height: bioTextHeight,
                            color: Colors.red,
                            child: Text(
                                dashboardDataResponse == null
                                    ? "Add your bio"
                                    : dashboardDataResponse
                                                .data.userDetails.bio ==
                                            ""
                                        ? "Add your bio"
                                        : dashboardDataResponse
                                            .data.userDetails.bio,
                                overflow: TextOverflow.clip,
                                textAlign: TextAlign.center),
                          ),
                        )
                      ]
                      )
                  ),

                  */
                ),
              ),
            ),

            // -- link
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(link_tab_icon, height: 12, width: 12),
                Text(
                    " https://my.lookz.me/${dashboardDataResponse == null ? "" : dashboardDataResponse.data.userDetails.userName}",
                    style: defaultStyle)
              ],
            ),

            // -- creation social media links
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                height: 25,
                color: Colors.grey.shade200,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(Languages.of(context).my_socialmedia,
                          style: GoogleFonts.openSans(
                              fontSize: 11, fontWeight: FontWeight.w500)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: GestureDetector(
                        child: Container(
                            height: 25,
                            width: 60,
                            child: Center(
                                child: Text(
                                    isHavingSocialLinks
                                        ? Languages.of(context).edit
                                        : Languages.of(context).create,
                                    style: GoogleFonts.openSans(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w500,
                                        color: mainThemeColor)))),
                        onTap: () {
                          Navigator.of(context)
                              .push(PageRouteBuilder(
                                  opaque: false,
                                  pageBuilder: (BuildContext context, _, __) =>
                                      CreateLinkScreen()))
                              .then((val) => val ? getHomeData(context) : null);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // -- creation social media links
            isHavingSocialLinks
                ? Container(
                    height: 35,
                    color: Colors.grey.shade200,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8),
                      child: ListView.builder(
                        itemCount: dashboardDataResponse == null
                            ? 0
                            : dashboardDataResponse.data.links.length,
                        itemBuilder: (context, index) {
                          return new Card(
                              color: Colors.transparent,
                              elevation: 0.0,
                              child: new Container(
                                decoration: BoxDecoration(
                                  color: Colors.black87,
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(link_tab_icon,
                                          color: Colors.white,
                                          height: 10,
                                          width: 10),
                                      SizedBox(width: 5),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 3),
                                        child: Text(
                                          dashboardDataResponse
                                              .data.links[index].linkName,
                                          style: GoogleFonts.openSans(
                                              color: Colors.white,
                                              fontSize: 12),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                alignment: Alignment.center,
                              ));
                        },
                        scrollDirection: Axis.horizontal,
                      ),
                    ),
                  )
                : Container(
                    height: 0,
                  ),

            isHavingSocialLinks
                ? Container(
                    height: 10,
                    color: Colors.grey.shade200,
                    width: MediaQuery.of(context).size.width,
                  )
                : Container(
                    height: 0,
                  ),
          ],
        ),
      ),
    );
  }

  // -- contact tabs
  returnContactsTabs() {
    return SliverPersistentHeader(
      delegate: _SliverAppBarDelegate(
        TabBar(
          indicatorColor: Colors.black,
          controller: _tabController,
          labelColor: mainThemeColor,
          unselectedLabelColor: Colors.white,
          tabs: [
            Tab(icon: SvgPicture.asset(profile_tab)),
            Tab(icon: SvgPicture.asset(distance_tab)),
          ],
          onTap: (index) {
            print("this-----");
            print(index);
            selectedSectionTab = index;

            if (index == 0) {
              getHomeData(context);
            } else {
              getDistanceContacts();
            }

            setState(() {});
          },
        ),
        selectedSectionTab,
      ),
      pinned: true,
      floating: false,
    );
  }

  // -- get dashboard api data
  getHomeData(BuildContext cntxt) async {
    showLoader(cntxt);
    print("1");
    Response searchedHashtagsResponse = await ApiController().httpControllerGet(
        HttpServicesList().getDashboardData + LocalStore.shared.userId, cntxt);
    if (searchedHashtagsResponse != null) {
      print("2");
      DashboardApiModel dashboardDataApi = DashboardApiModel.fromJson(
          json.decode(searchedHashtagsResponse.body));
      if (dashboardDataApi.status != null) {
        print("3");
        //_getAllHashtagsController.add(allHashTagsApi);
        dashboardDataResponse = dashboardDataApi;

        LocalStore.shared.userSocialLinks = dashboardDataApi.data.links;

        dashboardDataApi.data.links.length == 0
            ? isHavingSocialLinks = false
            : isHavingSocialLinks = true;

        print("bio length ----");

        LocalStore.shared.username =
            dashboardDataResponse.data.userDetails.userName == null
                ? ""
                : dashboardDataResponse.data.userDetails.userName;

        if (dashboardDataApi.data.userDetails.bio == null) {
          bioTextHeight = 20;
        } else {
          if (dashboardDataApi.data.userDetails.bio.length < 35) {
            bioTextHeight = 20;
          } else if (dashboardDataApi.data.userDetails.bio.length > 35 &&
              dashboardDataApi.data.userDetails.bio.length < 65) {
            bioTextHeight = 35;
          } else if (dashboardDataApi.data.userDetails.bio.length > 65 &&
              dashboardDataApi.data.userDetails.bio.length < 95) {
            bioTextHeight = 50;
          } else if (dashboardDataApi.data.userDetails.bio.length > 95 &&
              dashboardDataApi.data.userDetails.bio.length < 140) {
            bioTextHeight = 70;
          } else {
            bioTextHeight = 70;
          }
        }

        setState(() {});
        //hideLoader(cntxt);
      } else {
        print("4");
        hideLoader(cntxt);
        CustomAlertViewWidget().showSnackBar(cntxt, "Please try again");
      }
    } else {
      print("5");
      hideLoader(cntxt);
      CustomAlertViewWidget().showSnackBar(cntxt, "Please try again");
    }
  }

  // -- get dashboard distance contact api data
  DashboardDistanceContactApiModel dashboardDistanceContactApiModel;
  bool contactIsReportedMe = false;
  getNotiObserverEvents() {
    DartNotificationCenter.subscribe(channel: "refreshEvent", observer: 1, onNotification: (result)  {
      getDistanceContacts();
      // DartNotificationCenter.unsubscribe(observer: 1, channel: "refreshEvent");
      // DartNotificationCenter.unregisterChannel(channel: "refreshEvent");
    });
  }

  getDistanceContacts() async {
    print("dadadadasdas");
    showLoader(context);
    print("1");

    Response dashboardDistContactResponse = await ApiController()
        .httpControllerPost(
            HttpServicesList().getDashboardDistContact +
                LocalStore.shared.userId,
            null,
            "",
            context);
    print("2");
    if (dashboardDistContactResponse != null) {
      hideLoader(context);
      DashboardDistanceContactApiModel allDistContactApiResponse =
          DashboardDistanceContactApiModel.fromJson(
              json.decode(dashboardDistContactResponse.body));
      if (allDistContactApiResponse.status != null) {
        dashboardDistanceContactApiModel = allDistContactApiResponse;
        // dashboardDistanceContactApiModel.data.contacts.add(dashboardDistanceContactApiModel.data.contacts[0]);
        // dashboardDistanceContactApiModel.data.contacts.add(dashboardDistanceContactApiModel.data.contacts[0]);
        setState(() {});
      } else {
        print("4");
        CustomAlertViewWidget().showSnackBar(context, "Please try again");
      }
    } else {
      print("5");
      hideLoader(context);
      CustomAlertViewWidget().showSnackBar(context, "Please try again");
    }
  }

  _sendImageToCloud() async {
    showLoader(context);
    //     ---------->> image uploading to firebase
    FirebaseStorage storage = FirebaseStorage.instance;
    Reference ref = storage
        .ref()
        .child(LocalStore.shared.userId + DateTime.now().toString());
    UploadTask uploadTask = ref.putFile(imageFile);

    // var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
    // url = dowurl.toString();

    uploadTask.then((res) {
      res.ref.getDownloadURL();
      print(res.ref.getDownloadURL().whenComplete(() {
        res.ref.getDownloadURL().then((value) async {
          final Map<String, String> qrDict = {
            "imageUrl": value,
          };
          var statusString =
              await _userFireStoreMethodObj.updateDataToTableWithCustomId(
                  qrDict, "users", LocalStore.shared.userId);
          hideLoader(context);
          dashboardDataResponse.data.userDetails.userImage = value;
          //profileDataDict["imageUrl"] = value;
          setState(() {});
        });
      }));
    });
  }

  // -- image uploading
  Future getCameraImage(BuildContext context) async {
    try {
      final pickedFile = await picker.getImage(source: ImageSource.camera);
      _selectedImageFile = pickedFile.path;
      Navigator.pop(context);
      convertToBase64(File(pickedFile.path));
      imageFile = File(pickedFile.path);
      _sendImageToCloud();
    } on Exception catch (exception) {
      CustomAlertViewWidget().showTextAlertView(context, "Error",
          "Please allow for camera permissions in settings", "OK");
    } catch (error) {
      print("executed for errors of all types other than Exception");
    }
  }

  convertToBase64(File _image) {
    List<int> imageBytes = _image.readAsBytesSync();
    _selectedImageFile = base64Encode(imageBytes);
  }

  pickImageFromGallery(BuildContext context) async {
    try {
      final pickedFile = await picker.getImage(source: ImageSource.gallery);
      _selectedImageFile = pickedFile.path;
      Navigator.pop(context);
      convertToBase64(File(pickedFile.path));
      imageFile = File(pickedFile.path);
      _sendImageToCloud();
    } on Exception catch (exception) {
      CustomAlertViewWidget().showTextAlertView(context, "Error",
          "Please allow for photos permissions in settings", "OK");
    } catch (error) {
      print("executed for errors of all types other than Exception");
    }
  }

  //======================================
  //                       User blur
  //======================================
  Future<String> blurUser(String tableName, String otherUserId, int userIndex, BuildContext cntxt) async {
    final Map<String, String> qrDict = {
      "addedOn": formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', hh, ':', mm, ':', ss]),
      "addedOnTimeStamp": DateTime.now().millisecondsSinceEpoch.toString(),
      "otherUserId": otherUserId,
      "userId": LocalStore.shared.userId,
    };

    try {
      var ref = await UserFirestoreMethods()
          .firestoreInstance
          .collection(tableName)
          .doc();
      ref.set(qrDict);
      Future.delayed(const Duration(milliseconds: 500), () {
        dashboardDistanceContactApiModel.data.users[userIndex].blurStatus = "1";
        dashboardDistanceContactApiModel.data.users[userIndex].blurId = ref.id;
        setState(() {});
      });
    } catch (error) {
      hideLoader(cntxt);
      Future.delayed(const Duration(milliseconds: 500), () {
        CustomAlertViewWidget()
            .showSnackBar(cntxt, "Something went wrong please try again");
      });
    }
  }

  //======================================
  //                     User unblur
  //======================================
  Future<String> unblurUser(String tableName, String tableId) async {
    showLoader(context);
    var statusString = await _userFireStoreMethodObj.deleteDataToTableWithCustomId(tableName, tableId);
    hideLoader(context);
    return statusString;
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar, this.selectedIndex);

  final TabBar _tabBar;
  int selectedIndex;




  @override
  double get minExtent =>
      selectedIndex == 1 ? 75 : 50; //_tabBar.preferredSize.height;

  @override
  double get maxExtent =>
      selectedIndex == 1 ? 75 : 50; //_tabBar.preferredSize.height;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.white, width: 1.0),
              ),
            ),
          ),
          _tabBar,
          selectedIndex == 1
              ? Container(
                  height: 25,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text(
                          Languages.of(context).my_distance_contact,
                          style: GoogleFonts.openSans(fontSize: 12),
                        ),
                      ),
                      Container(
                        width: 100,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              child: Container(
                                width: 20,
                                color: Colors.transparent,
                                child: SvgPicture.asset(sort_up_icon,
                                    height: 15, width: 15),
                              ),
                              onTap: () {
                                print("tap tap");
                              },
                            ),
                            GestureDetector(
                              child: Container(
                                width: 80,
                                color: Colors.transparent,
                                child: Text(
                                  Languages.of(context).see_all,
                                  style: GoogleFonts.openSans(fontSize: 12),
                                ),
                              ),
                              onTap: () {
                                // CustomNavigators.pushOnly(context, DistanceContacts());

                                Navigator.of(context)
                                    .push(PageRouteBuilder(
                                        opaque: false,
                                        pageBuilder:
                                            (BuildContext context, _, __) =>
                                                DistanceContacts()))
                                    .then((result) {

                                      print(result);

                                      if(result != null) {
                                        if (result == "back") {
                                              if(selectedSectionTab == 1) {
                                                Future.delayed(Duration(milliseconds: 500),(){
                                                 print("chlda dekhna se");



                                                });

                                              }

                                        }

                                      }


                                });
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              : Container(
                  height: 0,
                )
        ],
      ),
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return true;
  }
}


class TooltipShapeBorder extends ShapeBorder {
  final double arrowWidth;
  final double arrowHeight;
  final double arrowArc;
  final double radius;

  TooltipShapeBorder({
    this.radius = 10.0,
    this.arrowWidth = 20.0,
    this.arrowHeight = 10.0,
    this.arrowArc = 0.0,
  }) : assert(arrowArc <= 1.0 && arrowArc >= 0.0);

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.only(bottom: arrowHeight);

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) => null;

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    rect = Rect.fromPoints(
        rect.topLeft, rect.bottomRight - Offset(0, arrowHeight));
    double x = arrowWidth, y = arrowHeight, r = 1 - arrowArc;
    return Path()
      ..addRRect(RRect.fromRectAndRadius(rect, Radius.circular(radius)))
      ..moveTo(rect.bottomCenter.dx + x / 2, rect.bottomCenter.dy)
      ..relativeLineTo(-x / 2 * r, y * r)
      ..relativeQuadraticBezierTo(
          -x / 2 * (1 - r), y * (1 - r), -x * (1 - r), 0)
      ..relativeLineTo(-x / 2 * r, -y * r);
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {}

  @override
  ShapeBorder scale(double t) => this;
}

