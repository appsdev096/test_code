import 'dart:async';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lookz_me/custom_widgets/action_button.dart';
import 'package:lookz_me/custom_widgets/qr_code_scanner_widget.dart';
import 'package:lookz_me/custom_widgets/qr_scanner.dart';
import 'package:lookz_me/firestore_methods/user_firestore_methods.dart';
import 'package:lookz_me/helper/constants.dart';
import 'package:lookz_me/helper/create_links_helpers.dart';
import 'package:lookz_me/helper/custom_alert_view.dart';
import 'package:lookz_me/helper/custom_loader.dart';
import 'package:lookz_me/helper/local_store.dart';
import 'package:lookz_me/languages/language.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:share/share.dart';

import '../../main.dart';

class SharePointScreen extends StatefulWidget {
  final selectedSharePointData;
  const SharePointScreen({Key key, this.selectedSharePointData})
      : super(key: key);
  @override
  _SharePointScreenState createState() => _SharePointScreenState();
}

extension NumberParsing on String {
  double toDouble() {
    return double.parse(this);
  }
}

class _SharePointScreenState extends State<SharePointScreen>
    with TickerProviderStateMixin {
  int selectedIndex = 0;
  String linkString = "";
  String distanceContactLinkString = "";
  bool isConnected = false;
  bool isRented = false;
  double _currentSliderValue = 20;
  TextEditingController hardwareSearchFieldController = TextEditingController();
  TextEditingController sharePointHardwareSearchFieldController =
  TextEditingController();
  List<String> daysArr = ["1", "7", "14", "30", "365"];
  int daySelectedIndex = 0;
  Map sharePointData = Map<String, String>();
  String hardwareUsedRangeVal = "0.0";
  String locationUsedRangeVal = "0.0";
  TabController _tabController;
  String endDateStr = "";
  Map hardwareDeviceInfoDict = Map<String, String>();
  Map mapTempDetails = Map<String, String>();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  static const platform = const MethodChannel('com.example.wrapscan');

  static const getScanplatform = const MethodChannel('com.example.getwrapscan');

  Future<void> _getEnteredDeviceDetails(String targetId) async {
    print("_getEnteredDeviceDetails");
    try {
      print("try-----");
      final result = await getScanplatform.invokeMethod(
          'getCustomScanning', targetId); // [ac233f7b1ef7, ac233f7a75fe]
      print("result");
      print(result);
      hardwareDeviceInfoDict = result;
      if (hardwareDeviceInfoDict['message'].toString() == "Bluetooth is off") {
        CustomAlertViewWidget().showTextAlertView(
            context, "Error!", "Your device bluetooth is off", "OK",
            okCompletion: (val) {});
      } else {
        print("result22");
        print(hardwareDeviceInfoDict['identifier']);
        print(hardwareDeviceInfoDict['message']);
        setState(() {
          print("????????????????????");
        });
      }
    } on PlatformException catch (e) {
      print("????????????????????");
      print("catch-----");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("-=-=-=-");

    hardwareDeviceInfoDict["status"] = "";
    _getUserLocation();
    linkString = CreateLinkHelper().createStoryUniqueLink(
        LocalStore.shared.username, LocalStore.shared.userId);
    distanceContactLinkString = CreateLinkHelper().createStoryUniqueLink(
        LocalStore.shared.username + "-Distance-", LocalStore.shared.userId);

    // if (widget.selectedSharePointData != null) {
    //   sharePointData = widget.selectedSharePointData;
    //   print("-*-*-/////-*-*-*-*////-*-*-*");
    //   print(sharePointData);
    //   if (sharePointData["sharePointType"] == "link") {
    //     selectedIndex = 0;
    //   } else if (sharePointData["sharePointType"] == "qr") {
    //     selectedIndex = 1;
    //   } else if (sharePointData["sharePointType"] == "hardware") {
    //     selectedIndex = 2;
    //   } else if (sharePointData["sharePointType"] == "location") {
    //     selectedIndex = 3;
    //   } else if (sharePointData["sharePointType"] == "distanceContact") {
    //     selectedIndex = 4;
    //   }
    //   // Future.delayed(Duration(seconds: 5), () {
    //   //   print("donedwedwdw");
    //   //   _tabController = TabController(length: 5, vsync: this, initialIndex: 3);
    //   //   _tabController.animateTo(3);
    //   // });
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: whiteColor,
      child: new DefaultTabController(
        length: 5,
        child: SafeArea(
          child: new Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            appBar: new PreferredSize(
              preferredSize: Size.fromHeight(100),
              child: Column(
                children: [
                  // -- app bar
                  Container(
                    height: 50,
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 5),
                          child: GestureDetector(
                            child: Container(
                                height: 35,
                                width: 35,
                                child: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.black,
                                )),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                        Text(
                          "Share Point",
                          style: GoogleFonts.openSans(
                              color: Colors.black, fontSize: 18),
                        ),
                        Icon(Icons.arrow_back_ios, color: Colors.white),
                      ],
                    ),
                  ),
                  // -- tab bar

                  Container(
                    height: 50.0,
                    child: new TabBar(
                      controller: _tabController,
                      indicatorColor: Colors.black,
                      onTap: (index) async {
                        selectedIndex = index;
                        clearDict();

                        if (selectedIndex == 2 || selectedIndex == 4) {
                          hardwareUsedRangeVal = "0.0";
                        }

                        if (selectedIndex == 3) {
                          bool isLocationServiceEnabled =
                          await Geolocator.isLocationServiceEnabled();
                          if (isLocationServiceEnabled == false) {
                            LocationPermission permission =
                            await Geolocator.requestPermission();
                          }
                        }
                        setState(() {});
                      },
                      tabs: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              link_icon,
                              height: 16,
                              width: 16,
                              color: selectedIndex == 0
                                  ? Colors.black
                                  : Colors.black38,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 3),
                              child: SvgPicture.asset(
                                selectedIndex == 0
                                    ? tab_select_tick_icon
                                    : cross_icon,
                                height: 13,
                                width: 13,
                                color: selectedIndex == 0
                                    ? Colors.green
                                    : Colors.red,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              barcodeIcon,
                              height: 16,
                              width: 16,
                              color: selectedIndex == 1
                                  ? Colors.black
                                  : Colors.black38,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 3),
                              child: SvgPicture.asset(
                                selectedIndex == 1
                                    ? tab_select_tick_icon
                                    : cross_icon,
                                height: 13,
                                width: 13,
                                color: selectedIndex == 1
                                    ? Colors.green
                                    : Colors.red,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              hardware_icon,
                              height: 16,
                              width: 16,
                              color: selectedIndex == 2
                                  ? Colors.black
                                  : Colors.black38,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 3),
                              child: SvgPicture.asset(
                                selectedIndex == 2
                                    ? tab_select_tick_icon
                                    : cross_icon,
                                height: 13,
                                width: 13,
                                color: selectedIndex == 2
                                    ? Colors.green
                                    : Colors.red,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              location_icon,
                              height: 16,
                              width: 16,
                              color: selectedIndex == 3
                                  ? Colors.black
                                  : Colors.black38,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 3),
                              child: SvgPicture.asset(
                                selectedIndex == 3
                                    ? tab_select_tick_icon
                                    : cross_icon,
                                height: 13,
                                width: 13,
                                color: selectedIndex == 3
                                    ? Colors.green
                                    : Colors.red,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              distance_contact_icon,
                              height: 16,
                              width: 16,
                              color: selectedIndex == 4
                                  ? Colors.black
                                  : Colors.black38,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 3),
                              child: SvgPicture.asset(
                                selectedIndex == 4
                                    ? tab_select_tick_icon
                                    : cross_icon,
                                height: 13,
                                width: 13,
                                color: selectedIndex == 4
                                    ? Colors.green
                                    : Colors.red,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            body: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                returnLinkConnectionBody(),
                returnQrCodeConnectionBody(),
                returnHardwareConnectionBody(),
                returnLocationConnectionBody(),
                returnDistanceContactBody(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // ==============
  //          LINK
  // ==============
  Widget returnLinkConnectionBody() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
          child: Container(
            height: 130,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      // "The following lookz.me\u00AE link is activated : ",
                      Languages.of(context).the_following_lookz + " " +  Languages.of(context).link + " " + Languages.of(context).is_activated,
                      maxLines: 2,
                      style: GoogleFonts.openSans(
                          fontWeight: FontWeight.w700, fontSize: 12),
                    )),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(linkString,
                          maxLines: 4,
                          style: GoogleFonts.openSans(fontSize: 13))),
                ),
              ],
            ),
          ),
        ),
        Container(
          height: 130,
          child: Column(
            children: [
              GestureDetector(
                child: Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width - 40,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(color: Colors.black, width: 1),
                      borderRadius: BorderRadius.circular(5)),
                  child: Center(
                      child: Text(
                        Languages.of(context).copy,
                        style: GoogleFonts.openSans(fontWeight: FontWeight.w700),
                      )),
                ),
                onTap: () {
                  Clipboard.setData(new ClipboardData(text: linkString))
                      .then((_) {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text(Languages.of(_scaffoldKey.currentContext).copy_copied,
                          style: GoogleFonts.openSans(color: Colors.white),
                          textAlign: TextAlign.center),
                      backgroundColor: mainThemeColor,
                    ));
                  });
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: ActionButton().actionButton(Languages.of(context).share, context, (value) {
                  FocusScope.of(context).unfocus();
                  print(value);
                  saveSharePointDetails("link", storyLink: linkString);
                  Navigator.pop(context, sharePointData);
                }, buttonState: true),
              )
            ],
          ),
        )
      ],
    );
  }

  // ================
  //          QR_CODE
  // ================
  Widget returnQrCodeConnectionBody() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
          child: Container(
            height: 130,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        Languages.of(context).the_following_lookz + " " +  Languages.of(context).qr_code + " " + Languages.of(context).is_activated,
                      maxLines: 2,
                      style: GoogleFonts.openSans(
                          fontWeight: FontWeight.w700, fontSize: 12),
                    )),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(linkString,
                          maxLines: 4,
                          style: GoogleFonts.openSans(fontSize: 13))),
                ),
              ],
            ),
          ),
        ),
        QrImage(
          data: linkString,
          version: QrVersions.auto,
          size: MediaQuery.of(context).size.width - 100,
        ),
        Container(
          height: 130,
          child: ActionButton().actionButton(Languages.of(context).share, context, (value) {
            FocusScope.of(context).unfocus();
            print(value);
            saveSharePointDetails("qr", storyLink: linkString);
            Navigator.pop(context, sharePointData);
          }, buttonState: true),
        )
      ],
    );
  }

  // =================
  //          HARDWARE
  // =================
  Widget returnHardwareConnectionBody() {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
      child: Container(
        height: MediaQuery.of(context).size.height - 200,
        child: ListView(
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                child: Text(
                  Languages.of(context).the_following_lookz + " " +  Languages.of(context).station + " " + Languages.of(context).is_activated,

                  maxLines: 2,
                  style: GoogleFonts.openSans(
                      fontWeight: FontWeight.w700, fontSize: 12),
                )),

            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                      Languages.of(context).hardware_tab_msg,
                      maxLines: 4,
                      style: GoogleFonts.openSans(fontSize: 13))),
            ),

            // -- text field with qr code
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                height: 35,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38, width: 1),
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Container(
                        width: 25,
                        child: SvgPicture.asset(
                          hardwareDeviceInfoDict['status'] == "true"
                              ? tab_select_tick_icon
                              : cross_icon,
                          color: hardwareSearchFieldController.text == ""
                              ? Colors.transparent
                              : Colors.black,
                          width: 20,
                          height: 20,
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 115,
                      child: TextFormField(
                          controller: hardwareSearchFieldController,
                          onTap: () {
                            print("object");
                          },
                          onFieldSubmitted: (onSubmitValue) {
                            _getEnteredDeviceDetails(
                                onSubmitValue.toLowerCase());
                            print(onSubmitValue);
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintStyle: GoogleFonts.openSans(
                                fontSize: 14, color: Colors.grey.shade400),
                            hintText: 'Search',
                            counterText: '',
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(
                                left: 5.0, bottom: 15, right: 5),
                          ),
                          style: GoogleFonts.openSans(
                              color: textColor, fontSize: 14)),
                    ),
                    GestureDetector(
                      child: Container(
                          height: 35,
                          width: 50,
                          color: Colors.transparent,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: SvgPicture.asset(barcodeIcon),
                          )),
                      onTap: () {
                        print("qr code");

                        Navigator.of(context)
                            .push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) =>
                                QRViewExample()))
                            .then((result) {
                          if (result != null) {
                            print(result.toString());
                            hardwareSearchFieldController.text =
                            result == null ? "" : result;
                            _getEnteredDeviceDetails(
                                hardwareSearchFieldController.text
                                    .toLowerCase());
                            Future.delayed(const Duration(milliseconds: 100),
                                    () {
                                  FocusScope.of(context).unfocus();
                                });
                          }
                        });
                      },
                    )
                  ],
                ),
              ),
            ),

            hardwareDeviceInfoDict['status'] == "true"
                ? Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 25),
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        Languages.of(context).following_device_detected,
                        maxLines: 2,
                        style: GoogleFonts.openSans(
                            fontWeight: FontWeight.w700, fontSize: 12),
                      )),
                ),

                // -- hardware icons
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 35,
                    child: Row(
                      children: [
                        SvgPicture.asset(hardware_3_dashboard_icon,
                            height: 27,
                            width: 27,
                            color: hardwareDeviceInfoDict['deviceName']
                                .toString() ==
                                "E2"
                                ? mainThemeColor
                                : mainThemeColorLite),
                        Padding(
                          padding:
                          const EdgeInsets.only(left: 40, right: 40),
                          child: SvgPicture.asset(
                              hardware_2_dashboard_icon,
                              height: 20,
                              width: 20,
                              color: hardwareDeviceInfoDict['deviceName']
                                  .toString() ==
                                  "I7"
                                  ? mainThemeColor
                                  : mainThemeColorLite),
                        ),
                        SvgPicture.asset(hardware_1_dashboard_icon,
                            height: 18,
                            width: 18,
                            color: hardwareDeviceInfoDict['deviceName']
                                .toString() ==
                                "Plus"
                                ? mainThemeColor
                                : mainThemeColorLite),
                      ],
                    ),
                  ),
                ),

                // -- device info
                Padding(
                  padding: const EdgeInsets.only(top: 25),
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        Languages.of(context).device_info,
                        style: GoogleFonts.openSans(
                            fontWeight: FontWeight.w700, fontSize: 12),
                      )),
                ),

                // -- id
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        "${Languages.of(context).id} ${hardwareDeviceInfoDict['identifier']}",
                        maxLines: 2,
                        style: GoogleFonts.openSans(fontSize: 12),
                      )),
                ),

                // -- distance
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                       "${Languages.of(context).distance_max}${getParticularDeviceRange()}",
                        style: GoogleFonts.openSans(
                            fontWeight: FontWeight.w700, fontSize: 12),
                      )),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        Languages.of(context).how_shold_far,
                        maxLines: 2,
                        style: GoogleFonts.openSans(fontSize: 12),
                      )),
                ),

                // -- slider
                Padding(
                  padding:
                  const EdgeInsets.only(top: 25, left: 15, right: 15),
                  child: Container(
                      child: FlutterSlider(
                        values: [0, 100],
                        min: 0,
                        max: 100,
                        handlerWidth: 20,
                        onDragging: (handlerVal, lowerVal, upperVal) {
                          hardwareUsedRangeVal = lowerVal.toString();
                        },
                        /*hatchMark: FlutterSliderHatchMark(
                      density: 0.5, // means 50 lines, from 0 to 100 percent
                      labels: [
                        FlutterSliderHatchMarkLabel(percent: 0, label: Text('0')),
                        FlutterSliderHatchMarkLabel(percent: 25, label: Text('25')),
                        FlutterSliderHatchMarkLabel(percent: 50, label: Text('50')),
                        FlutterSliderHatchMarkLabel(percent: 75, label: Text('75')),
                        FlutterSliderHatchMarkLabel(percent: 100, label: Text('100')),
                      ],
                    ),*/
                        trackBar: FlutterSliderTrackBar(
                          activeTrackBarHeight: 8,
                          inactiveTrackBarHeight: 8,
                          inactiveTrackBar: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.black12,
                          ),
                          activeTrackBar: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: mainThemeColor),
                        ),
                        tooltip: FlutterSliderTooltip(
                            alwaysShowTooltip: true,
                            textStyle:
                            GoogleFonts.openSans(color: Colors.black),
                            boxStyle: FlutterSliderTooltipBox(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                        color: mainThemeColor, width: 1)))),
                        handler: FlutterSliderHandler(
                            opacity: 0,
                            child: Container(
                              height: 20,
                              width: 20,
                              color: Colors.transparent,
                            )),
                      )),
                ),

                // -- connect button
                Padding(
                  padding: EdgeInsets.only(
                      bottom: 15,
                      top: MediaQuery.of(context).size.height / 10),
                  child: isConnected
                      ? GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20),
                      child: Container(
                        height: 40,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.black, width: 1),
                            borderRadius: BorderRadius.circular(5)),
                        child: Center(
                            child: Text(
                              Languages.of(context).disconnect,
                              style: GoogleFonts.openSans(
                                  fontWeight: FontWeight.w700),
                            )),
                      ),
                    ),
                    onTap: () {
                      isConnected = !isConnected;
                      setState(() {});
                    },
                  )
                      : Container(
                    height: 40,
                    child: ActionButton().actionButton(
                  Languages.of(context).connect, context, (value) async {
                      FocusScope.of(context).unfocus();
                      print(value);

                      if (hardwareUsedRangeVal == "0.0") {
                        CustomAlertViewWidget().showTextAlertView(
                            context,
                            Languages.of(context).error,
                            Languages.of(context).zero_distance_error,
                            Languages.of(context).ok,
                            okCompletion: (val) {});
                      } else {
                        checkHardwareIsLinked((value) {
                          if (value == false) {
                            print(
                                "55634326423642364235632---------------");
                            saveSharePointDetails("hardware",
                                storyLink: "",
                                hardwareId:
                                hardwareSearchFieldController
                                    .text
                                    .toLowerCase(),
                                hardwareTotalRange:
                                getParticularDeviceRange()
                                    .toString(),
                                hardwareType:
                                hardwareDeviceInfoDict[
                                'deviceName'],
                                hardwareUsedRange:
                                hardwareUsedRangeVal);
                            Navigator.pop(context, sharePointData);
                            //isConnected = !isConnected;
                            //setState(() {});
                          } else {
                            CustomAlertViewWidget()
                                .showTextAlertView(
                                context,
                                "Error",
                                "This device is already in use",
                                "Ok",
                                okCompletion: (val) {});
                          }
                        });
                      }
                    }, buttonState: true),
                  ),
                )
              ],
            )
                : Container(
                width: MediaQuery.of(context).size.width,
                child: Text(
                  hardwareDeviceInfoDict['status'] == "false"
                      ? "NO DEVICE DETECTED, PLEASE ENTER AGAIN."
                      : "",
                  maxLines: 2,
                  style: GoogleFonts.openSans(
                      fontWeight: FontWeight.w700,
                      fontSize: 12,
                      color: Colors.red),
                )),
          ],
        ),
      ),
    );
  }

  String getParticularDeviceRange() {
    if (hardwareDeviceInfoDict['deviceName'].toString() == "E2") {
      return "300m";
    }
    if (hardwareDeviceInfoDict['deviceName'].toString() == "I7") {
      return "100m";
    }
    if (hardwareDeviceInfoDict['deviceName'].toString() == "Plus") {
      return "30m";
    }
  }

  // =========================
  //          DISTANCE CONTACT
  // =========================
  Widget returnDistanceContactBody() {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
      child: Container(
        height: MediaQuery.of(context).size.height - 200,
        child: ListView(
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                child: Text(
                  Languages.of(context).the_following_lookz + " " +  Languages.of(context).distance_contact + " " + Languages.of(context).is_activated,

                  maxLines: 2,
                  style: GoogleFonts.openSans(
                      fontWeight: FontWeight.w700, fontSize: 12),
                )),

            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.shade300,
                    borderRadius: BorderRadius.circular(8)),
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, left: 15),
                  child: Text(
                    LocalStore.shared.username,
                    style: GoogleFonts.openSans(fontSize: 14),
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    Languages.of(context).distance_contact_msg,
                      maxLines: 4,
                      style: GoogleFonts.openSans(fontSize: 13))),
            ),

            // -- text field with qr code
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                height: 35,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38, width: 1),
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Container(
                        width: 25,
                        child: SvgPicture.asset(
                          hardwareDeviceInfoDict['status'] == "true"
                              ? tab_select_tick_icon
                              : cross_icon,
                          color:
                          sharePointHardwareSearchFieldController.text == ""
                              ? Colors.transparent
                              : Colors.black,
                          width: 20,
                          height: 20,
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 115,
                      child: TextFormField(
                          controller: sharePointHardwareSearchFieldController,
                          onTap: () {
                            print("object");
                          },
                          onFieldSubmitted: (onSubmitValue) {
                            _getEnteredDeviceDetails(
                                onSubmitValue.toLowerCase());
                            print(onSubmitValue);
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintStyle: GoogleFonts.openSans(
                                fontSize: 14, color: Colors.grey.shade400),
                            hintText: 'Search',
                            counterText: '',
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(
                                left: 5.0, bottom: 15, right: 5),
                          ),
                          style: GoogleFonts.openSans(
                              color: textColor, fontSize: 14)),
                    ),
                    GestureDetector(
                      child: Container(
                          height: 35,
                          width: 50,
                          color: Colors.transparent,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: SvgPicture.asset(barcodeIcon),
                          )),
                      onTap: () {
                        print("qr code");

                        Navigator.of(context)
                            .push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) =>
                                QRViewExample()))
                            .then((result) {
                          if (result != null) {
                            print(result.toString());
                            sharePointHardwareSearchFieldController.text =
                            result == null ? "" : result;
                            _getEnteredDeviceDetails(
                                sharePointHardwareSearchFieldController.text
                                    .toLowerCase());
                            Future.delayed(const Duration(milliseconds: 100),
                                    () {
                                  FocusScope.of(context).unfocus();
                                });
                          }
                        });
                      },
                    )
                  ],
                ),
              ),
            ),

            if (hardwareDeviceInfoDict['status'] == "true")
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 25),
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          Languages.of(context).following_device_detected,
                          maxLines: 2,
                          style: GoogleFonts.openSans(
                              fontWeight: FontWeight.w700, fontSize: 12),
                        )),
                  ),

                  // -- hardware icons
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 35,
                      child: Row(
                        children: [
                          SvgPicture.asset(hardware_3_dashboard_icon,
                              height: 27,
                              width: 27,
                              color: hardwareDeviceInfoDict['deviceName']
                                  .toString() ==
                                  "E2"
                                  ? mainThemeColor
                                  : mainThemeColorLite),
                          Padding(
                            padding: const EdgeInsets.only(left: 40, right: 40),
                            child: SvgPicture.asset(hardware_2_dashboard_icon,
                                height: 20,
                                width: 20,
                                color: hardwareDeviceInfoDict['deviceName']
                                    .toString() ==
                                    "I7"
                                    ? mainThemeColor
                                    : mainThemeColorLite),
                          ),
                          SvgPicture.asset(hardware_1_dashboard_icon,
                              height: 18,
                              width: 18,
                              color: hardwareDeviceInfoDict['deviceName']
                                  .toString() ==
                                  "Plus"
                                  ? mainThemeColor
                                  : mainThemeColorLite),
                        ],
                      ),
                    ),
                  ),

                  // -- device info
                  Padding(
                    padding: const EdgeInsets.only(top: 25),
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          Languages.of(context).device_info,
                          style: GoogleFonts.openSans(
                              fontWeight: FontWeight.w700, fontSize: 12),
                        )),
                  ),

                  // -- id
                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          "${Languages.of(context).id} ${hardwareDeviceInfoDict['identifier']}",
                          maxLines: 2,
                          style: GoogleFonts.openSans(fontSize: 12),
                        )),
                  ),

                  // -- distance
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          "${Languages.of(context).distance_max} ${getParticularDeviceRange()}",
                          style: GoogleFonts.openSans(
                              fontWeight: FontWeight.w700, fontSize: 12),
                        )),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          Languages.of(context).how_shold_far,
                          maxLines: 2,
                          style: GoogleFonts.openSans(fontSize: 12),
                        )),
                  ),

                  // -- slider
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 25, left: 15, right: 15),
                    child: Container(
                        child: FlutterSlider(
                          values: [0, 100],
                          min: 0,
                          max: 100,
                          handlerWidth: 20,
                          onDragging: (handlerVal, lowerVal, upperVal) {
                            hardwareUsedRangeVal = lowerVal.toString();
                          },
                          /*hatchMark: FlutterSliderHatchMark(
                      density: 0.5, // means 50 lines, from 0 to 100 percent
                      labels: [
                        FlutterSliderHatchMarkLabel(percent: 0, label: Text('0')),
                        FlutterSliderHatchMarkLabel(percent: 25, label: Text('25')),
                        FlutterSliderHatchMarkLabel(percent: 50, label: Text('50')),
                        FlutterSliderHatchMarkLabel(percent: 75, label: Text('75')),
                        FlutterSliderHatchMarkLabel(percent: 100, label: Text('100')),
                      ],
                    ),*/
                          trackBar: FlutterSliderTrackBar(
                            activeTrackBarHeight: 8,
                            inactiveTrackBarHeight: 8,
                            inactiveTrackBar: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.black12,
                            ),
                            activeTrackBar: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: mainThemeColor),
                          ),
                          tooltip: FlutterSliderTooltip(
                              alwaysShowTooltip: true,
                              textStyle: GoogleFonts.openSans(color: Colors.black),
                              boxStyle: FlutterSliderTooltipBox(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20),
                                      border: Border.all(
                                          color: mainThemeColor, width: 1)))),
                          handler: FlutterSliderHandler(
                              opacity: 0,
                              child: Container(
                                height: 20,
                                width: 20,
                                color: Colors.transparent,
                              )),
                        )),
                  ),

                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 40),
                        child: Container(
                          height: 70,
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(distanceContactLinkString,
                                        maxLines: 4,
                                        style: GoogleFonts.openSans(
                                            fontSize: 13))),
                              ),
                            ],
                          ),
                        ),
                      ),
                      QrImage(
                        data: distanceContactLinkString,
                        version: QrVersions.auto,
                        size: MediaQuery.of(context).size.width - 100,
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.only(top: 10, left: 20, right: 20),
                        child: GestureDetector(
                          child: Container(
                            height: 40,
                            width: MediaQuery.of(context).size.width - 30,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                border:
                                Border.all(color: Colors.black, width: 1),
                                borderRadius: BorderRadius.circular(5)),
                            child: Center(
                                child: Text(
                                  Languages.of(context).share_qr_code,
                                  style: GoogleFonts.openSans(
                                      fontWeight: FontWeight.w700),
                                )),
                          ),
                          onTap: () {
                            Clipboard.setData(
                                new ClipboardData(text: linkString))
                                .then((_) {
                              Share.share(distanceContactLinkString);
                              // _scaffoldKey.currentState.showSnackBar(SnackBar(
                              //   content: Text("Link copied to clipboard",
                              //       style: GoogleFonts.openSans(color: Colors.white),
                              //       textAlign: TextAlign.center),
                              //   backgroundColor: mainThemeColor,
                              // ));
                            });
                          },
                        ),
                      ),
                    ],
                  ),

                  // -- connect button
                  Padding(
                    padding: EdgeInsets.only(bottom: 50, top: 20),
                    child: isConnected
                        ? GestureDetector(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 0, right: 0),
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.black, width: 1),
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: Text(
                                "Disconnect",
                                style: GoogleFonts.openSans(
                                    fontWeight: FontWeight.w700),
                              )),
                        ),
                      ),
                      onTap: () {
                        isConnected = !isConnected;
                        setState(() {});
                      },
                    )
                        : Container(
                      height: 40,
                      child: ActionButton()
                          .actionButton(Languages.of(context).connect, context, (value) {
                        FocusScope.of(context).unfocus();
                        print(value);

                        if (hardwareUsedRangeVal == "0.0") {
                          CustomAlertViewWidget().showTextAlertView(
                              context,
                              Languages.of(context).error,
                              Languages.of(context).zero_distance_error,
                              Languages.of(context).ok,
                              okCompletion: (val) {});
                        } else {
                          checkHardwareIsLinked((value) {
                            if (value == false) {
                              saveSharePointDetails("distance_contact",
                                  storyLink: distanceContactLinkString,
                                  hardwareId:
                                  sharePointHardwareSearchFieldController
                                      .text
                                      .toLowerCase(),
                                  hardwareTotalRange:
                                  getParticularDeviceRange()
                                      .toString(),
                                  hardwareType: hardwareDeviceInfoDict[
                                  'deviceName'],
                                  hardwareUsedRange:
                                  hardwareUsedRangeVal);
                              Navigator.pop(context, sharePointData);
                            } else {
                              CustomAlertViewWidget().showTextAlertView(
                                  context,
                                  "Error",
                                  "This device is already in use",
                                  "Ok",
                                  okCompletion: (val) {});
                            }
                          });
                          //isConnected = !isConnected;
                          //setState(() {});
                        }
                      }, buttonState: true),
                    ),
                  )
                ],
              )
            else
              Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    hardwareDeviceInfoDict['status'] == "false"
                        ? "NO DEVICE DETECTED, PLEASE ENTER AGAIN."
                        : "",
                    maxLines: 2,
                    style: GoogleFonts.openSans(
                        fontWeight: FontWeight.w700,
                        fontSize: 12,
                        color: Colors.red),
                  )),
          ],
        ),
      ),
    );
  }

  checkHardwareIsLinked(Function(bool) completion) {
    showLoader(context);
    UserFirestoreMethods()
        .firestoreInstance
        .collection("stories")
        .where("share_point_details.hardwareId",
        isEqualTo: hardwareDeviceInfoDict['macId'].toString())
        .get()
        .then((event) {
      // -- if hardware doesn't exist
      hideLoader(context);
      print("---------------=======" + event.docs.toString());
      if (event.docs.length == 0) {
        completion(false);
      }
      // -- if hardware exists
      else {
        completion(true);
      }
    });

    print("-*-*-*-*-*-*-*-*-* 222");
  }

  // =================
  //          LOCATION
  // =================

  LatLng currentPostion;
  String currentLat = "";
  String currentLong = "";
  String currentAddress = "";

  void _getUserLocation() async {
    var position = await GeolocatorPlatform.instance
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    currentLat = position.latitude.toString();
    currentLong = position.longitude.toString();
    currentPostion = LatLng(position.latitude, position.longitude);

    print(currentLat);
    print(currentLong);
    print(currentPostion);

    Future.delayed(Duration(milliseconds: 200), () {
      setState(() {});
    });
  }

  void _getGeolocationAddress(double lat, double long) async {
    final coordinates = new Coordinates(lat, long);
    var addresses =
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    currentAddress = "${first.locality} ,${first.countryName}";
  }

  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(51.1657, 10.4515),
    zoom: 15,
  );

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  Widget returnLocationConnectionBody() {
    var newDate = new DateTime(
        DateTime.now().year, DateTime.now().month, DateTime.now().day + 1);
    endDateStr = newDate.toString();
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
      child: Container(
        height: MediaQuery.of(context).size.height - 200,
        child: ListView(
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                child: Text(
                    Languages.of(context).the_following_lookz + " " +  Languages.of(context).location.toUpperCase() + " " + Languages.of(context).is_activated,

                  maxLines: 2,
                  style: GoogleFonts.openSans(
                      fontWeight: FontWeight.w700, fontSize: 12),
                )),

            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    Languages.of(context).location_tab_msg,
                      maxLines: 4,
                      style: GoogleFonts.openSans(fontSize: 13))),
            ),

            // -- map view
            Padding(
              padding: const EdgeInsets.only(top: 15, bottom: 10),
              child: Stack(
                children: [
                  Container(
                    height: 350,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white,
                    child: GoogleMap(
                      mapType: MapType.normal,
                      gestureRecognizers: Set()
                        ..add(Factory<EagerGestureRecognizer>(
                                () => EagerGestureRecognizer())),
                      initialCameraPosition: currentPostion == null
                          ? _kGooglePlex
                          : CameraPosition(target: currentPostion, zoom: 15),
                      onMapCreated: (GoogleMapController controller) {
                        _controller.complete(controller);
                      },
                      onCameraIdle: () {
                        print("now ideal----------");
                        _getGeolocationAddress(double.parse(currentLat),
                            double.parse(currentLong));
                        print("----- " + currentAddress);
                      },
                      onCameraMove: (position) async {
                        mapTempDetails = position.toMap();
                        currentLat = mapTempDetails["target"][0].toString();
                        currentLong = mapTempDetails["target"][1].toString();

                        setState(() {});
                      },
                    ),
                  ),
                  Positioned(
                      top: 350 / 2 - 30,
                      left: MediaQuery.of(context).size.width / 2 - 30,
                      child: Container(
                        height: 30,
                        width: 30,
                        child: SvgPicture.asset(location_icon,
                            color: mainThemeColor),
                      ))
                ],
              ),

              //Image.asset("assets/common/map_dummy_img.jpg", height: 150, fit: BoxFit.cover),
            ),

            // -- device info
            Container(
                width: MediaQuery.of(context).size.width,
                child: Text(
                  Languages.of(context).location_info,
                  style: GoogleFonts.openSans(
                      fontWeight: FontWeight.w700, fontSize: 12),
                )),

            // -- id
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    "${currentLat}°, ${currentLong}°",
                    maxLines: 2,
                    style: GoogleFonts.openSans(fontSize: 12),
                  )),
            ),

            // -- distance
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(

                    Languages.of(context).distance_max + "2000m",

                    // "Distance max. : 2000m",
                    style: GoogleFonts.openSans(
                        fontWeight: FontWeight.w700, fontSize: 12),
                  )),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    Languages.of(context).how_shold_far,
                    maxLines: 2,
                    style: GoogleFonts.openSans(fontSize: 12),
                  )),
            ),

            // -- slider
            Padding(
              padding: const EdgeInsets.only(top: 25, left: 15, right: 15),
              child: Container(
                  child: FlutterSlider(
                    values: [double.parse(locationUsedRangeVal), 100],
                    min: 0,
                    max: 100,
                    handlerWidth: 20,
                    trackBar: FlutterSliderTrackBar(
                      activeTrackBarHeight: 8,
                      inactiveTrackBarHeight: 8,
                      inactiveTrackBar: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.black12,
                      ),
                      activeTrackBar: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: mainThemeColor),
                    ),
                    onDragging: (handlerVal, lowerVal, upperVal) {
                      locationUsedRangeVal = lowerVal.toString();
                    },
                    tooltip: FlutterSliderTooltip(
                        alwaysShowTooltip: true,
                        textStyle: GoogleFonts.openSans(color: Colors.black),
                        boxStyle: FlutterSliderTooltipBox(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20),
                                border:
                                Border.all(color: mainThemeColor, width: 1)))),
                    handler: FlutterSliderHandler(
                        opacity: 0,
                        child: Container(
                          height: 50,
                          width: 50,
                          color: Colors.transparent,
                        )),
                  )),
            ),

            isRented
                ? Padding(
              padding: const EdgeInsets.only(top: 5, left: 10, right: 15),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width - 70,
                    child: Text(
                      "Remaining rental period",
                      style: GoogleFonts.openSans(
                          fontWeight: FontWeight.w700, fontSize: 10),
                    ),
                  ),
                  LinearPercentIndicator(
                    animation: true,
                    lineHeight: 8,
                    animationDuration: 1500,
                    percent: 0.4,
                    linearStrokeCap: LinearStrokeCap.roundAll,
                    progressColor: mainThemeColor,
                    backgroundColor: Colors.black12,
                  ),
                ],
              ),
            )
                :
            // -- days
            Padding(
              padding:
              const EdgeInsets.only(top: 20, left: 10, right: 10),
              child: Container(
                height: 30,
                child: ListView.builder(
                  itemCount: 5,
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child: GestureDetector(
                        child: daySelectedIndex == index
                            ? Container(
                          height: 15,
                          width: 70,
                          child: Stack(
                            children: [
                              SvgPicture.asset(
                                  "assets/login_assets/button_bg.svg",
                                  fit: BoxFit.fill),
                              Center(
                                child: Center(
                                    child: Text(
                                        daysArr[index] +
                                            (daysArr[index] == "1"
                                                ? " " + Languages.of(context).day
                                                : " " + Languages.of(context).days),
                                        style: GoogleFonts.openSans(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontWeight:
                                            FontWeight.w600))),
                              ),
                            ],
                          ),
                        )
                            : Container(
                          height: 15,
                          width: 70,
                          child: Center(
                              child: Text(
                                  daysArr[index] +
                                      (daysArr[index] == "1"
                                          ? " " + Languages.of(context).day
                                          : " " + Languages.of(context).days),
                                  style: GoogleFonts.openSans(
                                      color: Colors.black87,
                                      fontSize: 15,
                                      fontWeight:
                                      FontWeight.w600))),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.black38)),
                        ),
                        onTap: () {
                          print(index);
                          daySelectedIndex = index;
                          var newDate = new DateTime(
                              DateTime.now().year,
                              DateTime.now().month,
                              DateTime.now().day +
                                  int.parse(daysArr[index]));
                          endDateStr = newDate.toString();
                          setState(() {});
                        },
                      ),
                    );
                  },
                  scrollDirection: Axis.horizontal,
                ),
              ),
            ),

            // -- rent button
            Padding(
              padding: EdgeInsets.only(bottom: 15, top: 30),
              child: isRented
                  ? GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(5)),
                    child: Center(
                        child: Text(
                          "End Rent",
                          style: GoogleFonts.openSans(
                              fontWeight: FontWeight.w700),
                        )),
                  ),
                ),
                onTap: () {
                  isRented = !isRented;
                  setState(() {});
                },
              )
                  : Container(
                height: 40,
                child: ActionButton()
                    .actionButton(Languages.of(context).rent_location, context, (value) {
                  FocusScope.of(context).unfocus();
                  print(value);
                  if (locationUsedRangeVal == "0.0") {
                    CustomAlertViewWidget().showTextAlertView(
                        context,  Languages.of(context).error,  Languages.of(context).zero_distance_error,  Languages.of(context).ok,
                        okCompletion: (val) {});
                  } else {
                    double distance = calculateDistance(
                        currentPostion.latitude,
                        currentPostion.longitude,
                        currentLat.toDouble(),
                        currentLong.toDouble());
                    if ((distance * 1000.0) > 100) {
                      CustomAlertViewWidget().showTextAlertView(
                          context,
                          Languages.of(context).error,
                          Languages.of(context).location_error,
                          Languages.of(context).ok,
                          okCompletion: (val) {});
                    } else {
                      saveSharePointDetails("location",
                          locationLat: currentLat,
                          locationLong: currentLong,
                          location: currentAddress,
                          locationTotalRange: "2000",
                          locationUsedRange: locationUsedRangeVal,
                          rentPeriod: daysArr[daySelectedIndex],
                          endDateStr: endDateStr);
                      Navigator.pop(context, sharePointData);
                    }

                    //isRented = !isRented;
                    //setState(() {});
                  }
                }, buttonState: true),
              ),
            )
          ],
        ),
      ),
    );
  }

  clearDict() {
    sharePointData = Map<String, String>();
  }

  saveSharePointDetails(String sharePointType,
      {String storyLink = "",
        String hardwareId = "",
        String hardwareTotalRange = "",
        String hardwareUsedRange = "",
        String hardwareType = "",
        String locationLat = "",
        String locationLong = "",
        String location = "",
        String locationTotalRange = "",
        String locationUsedRange = "",
        String rentPeriod = "",
        String endDateStr = ""}) {
    sharePointData["sharePointType"] = sharePointType;
    sharePointData["sharePointLink"] = storyLink;
    sharePointData["hardwareId"] = hardwareId;
    sharePointData["hardwareTotalRange"] = hardwareTotalRange;
    sharePointData["hardwareUsedRange"] = hardwareUsedRange;
    sharePointData["hardwareType"] = hardwareType;
    sharePointData["locationLat"] = locationLat;
    sharePointData["locationLong"] = locationLong;
    sharePointData["location"] = location;
    sharePointData["locationTotalRange"] = locationTotalRange;
    sharePointData["locationUsedRange"] = locationUsedRange;
    sharePointData["rentDays"] = rentPeriod;
    sharePointData["endDate"] = endDateStr;
  }
}
