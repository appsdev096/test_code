import UIKit
import Flutter
import Firebase
import MTBeaconPlus
import GoogleMaps

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  
    var manager = MTCentralManager.sharedInstance()!
    var scannerDevices : Array<MTPeripheral> = []
    var tempScannerDevices : Array<MTPeripheral> = []
    var scannedDevicesMacAddress = [String]()
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    FirebaseApp.configure()
    
    GMSServices.provideAPIKey("AIzaSyC-urjSlU6TA9k3mS6qRCvPswMVRHjZ9wQ")
//        GeneratedPluginRegistrant.register(with: self)
    
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
    }
    
   let batteryChannel = FlutterMethodChannel(name: "com.example.wrapscan",
                                             binaryMessenger: controller.binaryMessenger)

             batteryChannel.setMethodCallHandler({
             [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in

             // Note: this method is invoked on the UI thread.
             guard call.method == "getBatteryLevel" else {
              result(FlutterMethodNotImplemented)
              return
             }
           self?.receiveBatteryLevel(result: result)
         })


    let startCustomScanning = FlutterMethodChannel(name: "com.example.wrapscan",
                                              binaryMessenger: controller.binaryMessenger)

    startCustomScanning.setMethodCallHandler({
              [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in

              // Note: this method is invoked on the UI thread.
              guard call.method == "startCustomScanning" else {
               result(FlutterMethodNotImplemented)
               return
              }
            self?.startCustomScanningMethod(result: result)
          })


    let getCustomScanning = FlutterMethodChannel(name: "com.example.getwrapscan",
                                            binaryMessenger: controller.binaryMessenger)

    getCustomScanning.setMethodCallHandler({
        [weak self] (call: FlutterMethodCall, result: FlutterResult ) -> Void in


              // Note: this method is invoked on the UI thread.
              guard call.method == "getCustomScanning" else {
               result(FlutterMethodNotImplemented)
               return
              }
        self?.getCustomScanning(result: result, deviceId: call.arguments as! String)
          })

    
    
  

    
    
            startScanning()
    
    
    
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    func startScanning()  {
        
        
        
        
        
//        self.manager.stateBlock = { (state) in
//            if state != .poweredOn {
//                print("the iphone bluetooth state error")
//                // Return no device connnected.
//            }
//
//            else {
//                self.manager.startScan { (devices) in
//                    self.scannerDevices = self.manager.scannedPeris
//                }
//            }
//        }
        
        
        self.manager.startScan { (devices) in
            self.scannerDevices = self.manager.scannedPeris
        }
        
        
        
       
    }
    
    func stopScan() {
        manager.stopScan()
    }
    
    
    private func startCustomScanningMethod(result: FlutterResult) {
        startScanning()
        result("for scanning data")
        
    }
   
 
    
    private func getCustomScanning(result: FlutterResult, deviceId: String) {
        
        
        
        if self.manager.state == .poweredOff {
            let deviceInfoDict:NSDictionary = [
                "macId" : deviceId,
                "deviceName" : "",
                "identifier" : "",
                "status" : "false",
                "message" : "Bluetooth is off"
            ]
            result(deviceInfoDict)
        }
        else if self.manager.state == .unauthorized {
            let deviceInfoDict:NSDictionary = [
                "macId" : deviceId,
                "deviceName" : "",
                "identifier" : "",
                "status" : "false",
                "message" : "Bluetooth is unauthorized"
            ]
            result(deviceInfoDict)
        }
        else if self.manager.state == .unsupported {
            let deviceInfoDict:NSDictionary = [
                "macId" : deviceId,
                "deviceName" : "",
                "identifier" : "",
                "status" : "false",
                "message" : "Bluetooth is unsupported"
            ]
            result(deviceInfoDict)
        }
        
        else {
            self.scannedDevicesMacAddress.removeAll()
            tempScannerDevices  = self.scannerDevices
            
            for device in tempScannerDevices {
               self.scannedDevicesMacAddress.append(device.framer.mac ?? "")
            }
            
            if (self.scannedDevicesMacAddress.contains(""))  {
                let deviceInfoDict:NSDictionary = [
                    "macId" : deviceId,
                    "deviceName" : "",
                    "identifier" : "",
                    "status" : "false",
                    "message" : "This device not found"
                ]
                result(deviceInfoDict)
            }
            else {
                
                if  deviceId == "--"{
                    result(self.scannedDevicesMacAddress)
                }
                else {
                    if self.scannedDevicesMacAddress.contains(deviceId) {
                        var deviceIdIndex = Int()
                        deviceIdIndex = self.scannedDevicesMacAddress.firstIndex(of: deviceId)!
                        let framer = tempScannerDevices[deviceIdIndex].framer
                        
                        let deviceInfoDict:NSDictionary = [
                            "macId" : deviceId,
                            "deviceName" : framer?.name as Any,
                            "identifier" : tempScannerDevices[deviceIdIndex].identifier!,
                            "status" : "true"
                        ]
                        result(deviceInfoDict)
                    }
                    else {
                        let deviceInfoDict:NSDictionary = [
                        "macId" : deviceId,
                        "deviceName" : "",
                        "identifier" : "",
                        "status" : "false",
                        "message" : "This device not found"
                    ]
                    result(deviceInfoDict)}
                }
            }
        }
    }
    
    private func receiveBatteryLevel(result: FlutterResult) {
        print("receiveBatteryLevel----")
      let device = UIDevice.current
      device.isBatteryMonitoringEnabled = true
      if device.batteryState == UIDevice.BatteryState.unknown {
        result(FlutterError(code: "UNAVAILABLE",
                            message: "Battery info unavailable",
                            details: nil))
      }
      else {
        result(self.scannedDevicesMacAddress)
      }
    }
    
    
    private func getStaticString(result: FlutterResult) {
        result("receiveBatteryLevel----")
    }
    
    
}
